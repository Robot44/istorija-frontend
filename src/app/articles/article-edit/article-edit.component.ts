import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params  } from '@angular/router';

import { Article } from '../article';
import { ArticlesService } from '../articles.service';

import { FlashMessageService } from 'app/shared/flash-message/flash-message.service';

@Component({
    selector: 'article-edit',
    templateUrl: 'article-edit.component.html'
})
export class ArticleEditComponent implements OnInit {
    constructor(
        private fb: FormBuilder,
        private articleService: ArticlesService,
        private router: Router,
        private route: ActivatedRoute,
        private flashMessageService: FlashMessageService,
    ) {
        this.createForm();
    }

    private articleId: String;
    private isLoading: boolean;

    article: Article;
    articleForm: FormGroup;
    submitted: boolean = false;
    title: AbstractControl;
    content: AbstractControl;
    
    onSubmit() {
        this.submitted = true;
        if(this.articleForm.valid) {
            this.articleService.putArticle(this.articleId, JSON.stringify(this.articleForm.value))
            .then(
                data => {
                    this.flashMessageService.show('Article updated successfully!', 'success');
                    this.refresh();
                },
                error => {
                    this.flashMessageService.show('Failed to update article!', 'error');
                    this.refresh();
                }
            ); 
        }
    }

    ngOnInit(): void {
        this.route.params.subscribe(
            params => {
                this.articleId = params['id'];
                this.loadArticle();
            },
            error => {
                this.flashMessageService.show('Failed to load article!', 'error');
            }
        );
    }

    loadArticle() {
        this.isLoading = true;
        this.articleService.getArticle(this.articleId)
        .then(
            data => {
                this.article = data;
                this.title.setValue(data.title);
                this.content.setValue(data.content);
                this.isLoading = false;
            },
            error => {
                this.flashMessageService.show('Failed to load article!', 'error');
                this.refresh();
            }
        );
    }

    createForm() {
        this.articleForm = this.fb.group({
            title:['', Validators.compose([Validators.required, Validators.maxLength(40)])],
            content:['',  Validators.required],
        });
        this.title = this.articleForm.controls['title'];
        this.content = this.articleForm.controls['content'];
    }

    private refresh() {
        this.router.navigate(['/articles']);
    }
}