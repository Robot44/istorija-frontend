export interface Article {
    title: string;
    content: any;
    id: number;
}