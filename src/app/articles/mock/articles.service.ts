import { Injectable } from '@angular/core';
import { URLSearchParams, Headers, Http } from '@angular/http';

import { JwtHttp } from 'angular2-jwt-refresh';
import 'rxjs/add/operator/toPromise';
import { GlobalService } from "app/shared/global.service";
import { ArticlesService } from './../articles.service';

@Injectable()
export class MockArticleService extends ArticlesService {
    constructor() {
        super(null, null, null);
    }

    getArticle(id) {
        return Promise.resolve({title: "title", content: "some content", id: 1});
    }

    getPage(page) {
        return Promise.resolve({
            articles: [
                {title: "title", id: 1},
                {title: "title", id: 2}
            ],
            page: 1,
            total: 2,
            limit: 5
        });
    }

    removeArticle(id) {
        return Promise.resolve({title: "title", content: "some content", id: 1});
    }

    getArticles(timelineId, eventId) {
        return Promise.resolve({title: "title", content: "some content", id: 1});
    }

    getPublicArticle(articleId) {
        return Promise.resolve({title: "title", content: "some content", id: 1});
    }

    getAvailableArticles(timelineId, eventId) {
        return Promise.resolve([
            {title: "title", id: 1},
            {title: "title", id: 2},
            {title: "title", id: 3},
            {title: "title", id: 4}
        ]);
    }

    assignArticle(timelineId, eventId, articleId) {
        return Promise.resolve({});
    }

    unassignArticle(timelineId, eventId, articleId) {
        return Promise.resolve({});
    }

    postArticle(articleJson) {
        return Promise.resolve({title: "title", content: "some content", id: 1});
    }

    putArticle(articleId, articleJson) {
        return Promise.resolve({title: "title", content: "some content", id: 1});
    }
}