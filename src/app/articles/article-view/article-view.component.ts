import { Component, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';

import { Article } from '../article';
import { ArticlesService } from '../articles.service';

import { FlashMessageService } from '../../shared/flash-message/flash-message.service';

@Component({
    selector: 'article-view',
    templateUrl: './article-view.component.html',
    styleUrls: ['./article-view.component.scss']
})
export class ArticleViewComponent {
    constructor(
        private sanitizer: DomSanitizer,
        private route: ActivatedRoute,
        private articleService: ArticlesService,
        private flashMessageService: FlashMessageService,
        private router: Router,
    ) { }

    private article: Article;
    private isLoading: boolean;

    loadArticle(articleId: String) {
        this.isLoading = true;
        this.articleService.getPublicArticle(articleId)
        .then(
            data => {
                this.article = data;
                if(this.article.content) {
                    this.article.content = this.sanitizer.bypassSecurityTrustHtml(this.article.content);
                } 
                this.isLoading = false;
            },
            error => {
                this.flashMessageService.show('Failed to load article!', 'error');
            }
        );
    }

    clear() {
        this.article = null;
    }
}