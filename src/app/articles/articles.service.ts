import { Injectable } from '@angular/core';
import { URLSearchParams, Headers, Http } from '@angular/http';

import { JwtHttp } from 'angular2-jwt-refresh';
import 'rxjs/add/operator/toPromise';
import { GlobalService } from "app/shared/global.service";

@Injectable()
export class ArticlesService {
    constructor(
        public authHttp: JwtHttp,
        private http: Http,
        private global: GlobalService
    ) {
        if(global) {
            this.baseArticleUrl = this.global.getConfig().baseArticleUrl;
            this.baseUrl = this.global.getConfig().baseUrl;
        }
    }

    private baseArticleUrl: string;
    private baseUrl:string;

    getArticle(id) {
        return this.authHttp.get(this.baseArticleUrl + '/' + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getPage(page) {
        let body = new URLSearchParams();
        body.set('_page', page.toString());
        return this.authHttp.get(this.baseArticleUrl, { search : body})
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
    }

    removeArticle(id) {
        return this.authHttp.delete(this.baseArticleUrl + '/' + id)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
    }

    getArticles(timelineId, eventId) {
        return this.authHttp.get(this.baseUrl + 'timelines/' + timelineId + '/events/' + eventId + '/articles')
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
    }

    getPublicArticle(articleId) {
        return this.http.get(this.baseUrl + 'articles/' + articleId)
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
    }

    getAvailableArticles(timelineId, eventId) {
        return this.authHttp.get(this.baseUrl + 'timelines/' + timelineId + '/events/' + eventId + '/articles/available')
        .toPromise()
        .then(response => response.json())
        .catch(this.handleError);
    }

    assignArticle(timelineId, eventId, articleId) {
         return this.authHttp.post(this.baseUrl + 'timelines/' + timelineId + '/events/' + eventId + '/articles/' + articleId,'')
        .toPromise()
        .then(response => response)
        .catch(this.handleError);
    }

    unassignArticle(timelineId, eventId, articleId) {
         return this.authHttp.delete(this.baseUrl + 'timelines/' + timelineId + '/events/' + eventId + '/articles/' + articleId)
        .toPromise()
        .then(response => response)
        .catch(this.handleError);
    }

    postArticle(articleJson) {
        let myHeader = new Headers();
        myHeader.append('Content-Type', 'application/json');

        return this.authHttp.post(this.baseArticleUrl, articleJson, { headers: myHeader })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    putArticle(articleId, articleJson) {
        let myHeader = new Headers();
        myHeader.append('Content-Type', 'application/json');
        
        return this.authHttp.put(this.baseArticleUrl + '/' + articleId, articleJson, { headers: myHeader })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}