import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { ArticleCreateComponent } from './article-create.component';
import { FlashMessageService } from 'app/shared/flash-message/flash-message.service';
import { MockFlashMessageService } from 'app/shared/mock/flash-message.service';

import { ModalModule, PaginationModule, TabsModule, TimepickerModule } from 'ngx-bootstrap';
import { CalendarModule } from 'primeng/primeng';
import { ArticlesService } from '../articles.service';
import { MockArticleService } from '../mock/articles.service';

import { TinymceModule } from 'angular2-tinymce';

import { Router, ActivatedRoute  } from '@angular/router';
      
describe('ArticleCreateComponent', () => {
  let component: ArticleCreateComponent;
  let fixture: ComponentFixture<ArticleCreateComponent>;
  let articleService: ArticlesService;

  let mockRouter = {
      navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
      TestBed.configureTestingModule({
        imports: [
            ModalModule.forRoot(),
            FormsModule,
            ReactiveFormsModule,
            CommonModule,
            TabsModule.forRoot(),
            TinymceModule.withConfig({
            }),
        ],
        declarations: [ArticleCreateComponent],
        providers: [
            {provide: FlashMessageService, useClass: MockFlashMessageService},
            {provide: ArticlesService, useClass: MockArticleService},
            {provide: Router, useValue: mockRouter}
        ],
        schemas: [NO_ERRORS_SCHEMA]    
      })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleCreateComponent);
    component = fixture.componentInstance;
    articleService = TestBed.get(ArticlesService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should submit correct data', () => {
    spyOn(articleService, 'postArticle').and.callThrough();
    component.content.setValue('<H>asdfasdfasdfasdfas</H>');
    component.title.setValue('asdfasdf');
    component.onSubmit();
    fixture.whenStable().then(()=>{
        expect(articleService.postArticle).toHaveBeenCalledWith(JSON.stringify({title:"asdfasdf", content:'<H>asdfasdfasdfasdfas</H>'}));
    });
  });

  it('should be invalid', () => {
    component.content.setValue('');
    component.title.setValue('');
    fixture.detectChanges();
    fixture.whenStable().then(()=>{
        expect(component.articleForm.valid).toBeFalsy();
    });
  }); 
});
