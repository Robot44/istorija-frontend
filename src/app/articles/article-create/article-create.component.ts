import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';
import { Router, ActivatedRoute, Params  } from '@angular/router';

import { Article } from '../article';
import { ArticlesService } from '../articles.service';

import { FlashMessageService } from 'app/shared/flash-message/flash-message.service';

@Component({
    selector: 'article-create',
    templateUrl: 'article-create.component.html'
})
export class ArticleCreateComponent {
    article: Article;
    articleForm: FormGroup;
    submitted = false;
    title: AbstractControl;
    content: AbstractControl;
    isLoading: boolean = false;

    constructor(
        private fb: FormBuilder,
        private flashMessageService: FlashMessageService,
        private router: Router,
        private articleService: ArticlesService
    ) {
        this.createForm();
    }

    onSubmit() {
        this.submitted = true;
        this.article = this.articleForm.value;
        if(this.articleForm.valid) {
            this.isLoading = true;
            this.articleService.postArticle(JSON.stringify(this.articleForm.value))
            .then(
                data => {
                    this.flashMessageService.show('Article created successfully!', 'success');
                    this.refresh();
                },
                error => {
                    this.flashMessageService.show('Failed to create article!', 'error');
                    this.refresh();
                }
            );
        }
    }

    createForm() {
        this.articleForm = this.fb.group({
            title:['', Validators.compose([Validators.required, Validators.maxLength(40)])],
            content:['<p></p>', Validators.required],
        });
        this.title = this.articleForm.controls['title'];
        this.content = this.articleForm.controls['content'];
    }

    private refresh() {
        this.router.navigate(['/articles']);
    }
}