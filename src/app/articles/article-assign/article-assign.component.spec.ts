import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { ArticleAssignComponent } from './article-assign.component';

import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { ModalModule, PaginationModule, TabsModule, TimepickerModule } from 'ngx-bootstrap';
import { CalendarModule } from 'primeng/primeng';
import { ArticlesService } from '../articles.service';
import { MockArticleService } from '../mock/articles.service';

import { SelectModule } from 'ng2-select';
import { Event } from "app/events/event";

describe('ArticleAssignComponent', () => {
  let component: ArticleAssignComponent;
  let fixture: ComponentFixture<ArticleAssignComponent>;
  let articleService: ArticlesService;

  beforeEach(async(() => {
      TestBed.configureTestingModule({
        imports: [
            ModalModule.forRoot(),
            FormsModule,
            ReactiveFormsModule,
            CommonModule,
            PaginationModule.forRoot(),
            SelectModule,
            TabsModule.forRoot(),
            TimepickerModule.forRoot(),
            CalendarModule
        ],
        declarations: [ArticleAssignComponent],
        providers: [
            {provide: ArticlesService, useClass: MockArticleService},
            ],
        schemas: [NO_ERRORS_SCHEMA]    
      })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleAssignComponent);
    component = fixture.componentInstance;
    articleService = fixture.debugElement.injector.get(ArticlesService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should call unassign', () => {
    component.showModal(1,2);
    let spy = spyOn(articleService, 'unassignArticle').and.callThrough();
    component.selectedAssigned  = {title: "title", content: "some content", id: 2};
    component.remove();
    fixture.whenStable().then(()=>{

        expect(spy).toHaveBeenCalledWith(1,2,2);
    });
  });

  it('should call assign', () => {
    component.showModal(1,2);
    let spy = spyOn(articleService, 'assignArticle').and.callThrough();
    component.selectedAvailable  = {title: "title", content: "some content", id: 3};
    component.add();
    fixture.whenStable().then(()=>{
        expect(spy).toHaveBeenCalledWith(1,2,3);
    });
  }); 
});
