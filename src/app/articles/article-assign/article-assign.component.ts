import { Component, ViewChild } from '@angular/core';
import { Form } from '@angular/forms';

import { ModalDirective } from 'ngx-bootstrap';

import { ArticlesService } from '../articles.service';
import { Article } from '../article';

@Component({
    selector: 'article-assign',
    templateUrl: 'article-assign.component.html',
    styleUrls: ['./article-assign.component.scss']
})
export class ArticleAssignComponent {
    constructor(
        private articleService: ArticlesService
    ) { }

    @ViewChild(ModalDirective) modal : ModalDirective;

    assignedArticles: Array<Article> = [];
    availableArticles: Array<Article> = [];
    selectedAssigned: Article = null;
    selectedAvailable: Article = null;
    timelineId: number = null;
    eventId: number = null;

    showModal(timelineId, eventId) {
        this.timelineId = timelineId;
        this.eventId = eventId;
        this.getArticles();
        this.modal.show();
    }

    add(){
        if(this.selectedAvailable) {
            this.articleService.assignArticle(this.timelineId, this.eventId, this.selectedAvailable.id)
            .then(
                success => this.getArticles()
            );
        }
    }

    remove(){
        if(this.selectedAssigned) {
            this.articleService.unassignArticle(this.timelineId, this.eventId, this.selectedAssigned.id)
            .then(
                success => this.getArticles()
            );
        }
    }

    getArticles() {
        this.articleService.getArticles(this.timelineId, this.eventId)
        .then(
            data => this.assignedArticles = data
        );
        this.articleService.getAvailableArticles(this.timelineId, this.eventId)
        .then(
            data => this.availableArticles = data
        );
    }
}