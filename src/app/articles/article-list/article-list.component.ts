import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { FlashMessageService } from 'app/shared/flash-message/flash-message.service';

import { ArticlesService } from '../articles.service';

@Component({
    selector: 'article-list',
    templateUrl: 'article-list.component.html'
})
export class ArticleListComponent implements OnInit {
    public totalItems: number = 0;
    public currentPage: number = 1;
    public itemsPerPage: number = 0;
    public items: Array<any> = [];
    public isLoading: boolean;

    constructor(
        private flashMessageService: FlashMessageService,
        private router: Router,
        private articlesService: ArticlesService
    ) { }

    ngOnInit() { 
        this.getPage(this.currentPage);
    }

    getPage(page){
        this.isLoading = true;
        this.articlesService.getPage(page)
        .then(
            data => {
                this.items = data.articles;
                this.totalItems = data.total;
                this.itemsPerPage = data.limit;
                this.isLoading = false;
            },
            () => this.flashMessageService.show('Failed to fetch articles!', 'error')
        );
        
        return page;
    }

    removeArticle(event) {
        let target = event.currentTarget;
        let id = target.attributes.id.value;
        this.articlesService.removeArticle(id)
        .then(
            () => {
                this.flashMessageService.show('Article removed successfully!', 'success');
                this.getPage(this.currentPage);
            },
            () => this.flashMessageService.show('Failed to remove article!', 'error')
        );
    }

    private pageChanged(event: any): void {
        this.getPage(event.page);
    }
}