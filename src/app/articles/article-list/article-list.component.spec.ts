import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { Observable } from 'rxjs/Rx';

import { ArticleListComponent } from './article-list.component';
import { FlashMessageService } from 'app/shared/flash-message/flash-message.service';
import { MockFlashMessageService } from 'app/shared/mock/flash-message.service';

import { ModalModule, PaginationModule, TabsModule, TimepickerModule } from 'ngx-bootstrap';
import { ArticlesService } from '../articles.service';
import { MockArticleService } from '../mock/articles.service';

import { TinymceModule } from 'angular2-tinymce';

import { Router, ActivatedRoute } from '@angular/router';

describe('ArticleListComponent', () => {
  let component: ArticleListComponent;
  let fixture: ComponentFixture<ArticleListComponent>;
  let articleService: ArticlesService;

  let mockRouter = {
      navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
      TestBed.configureTestingModule({
        imports: [
            ModalModule.forRoot(),
            FormsModule,
            ReactiveFormsModule,
            CommonModule,
            TabsModule.forRoot(),
            PaginationModule.forRoot(),
            TinymceModule.withConfig({
            }),
        ],
        declarations: [ArticleListComponent],
        providers: [
            {provide: FlashMessageService, useClass: MockFlashMessageService},
            {provide: ArticlesService, useClass: MockArticleService},
            {provide: Router, useValue: mockRouter}
        ],
        schemas: [NO_ERRORS_SCHEMA]    
      })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ArticleListComponent);
    component = fixture.componentInstance;
    articleService = TestBed.get(ArticlesService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have two elements in list', async(() => {
    let spy = spyOn(articleService, 'getPage').and.callThrough();
    component.ngOnInit();
    fixture.detectChanges();
    fixture.whenStable().then(() => {
      fixture.detectChanges();
      expect(fixture.nativeElement.querySelectorAll("tbody tr").length).toBe(2);
      expect(spy).toHaveBeenCalled();
    });
  }));
  
  it('should call service delete on click', fakeAsync(() => {
    let spy = spyOn(articleService, 'removeArticle').and.callThrough();
    component.ngOnInit();
    tick();
    fixture.detectChanges();
    let element = fixture.nativeElement.querySelector('.remove');
    element.click();
    tick();
    fixture.detectChanges();
    expect(spy).toHaveBeenCalledWith('1');
  }));
});
