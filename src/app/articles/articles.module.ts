import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';

import { ArticleListComponent } from './article-list/article-list.component';
import { ArticleListRoutingModule } from './article-routing.module';
import { ArticlesService } from './articles.service';
import { ArticleCreateComponent } from './article-create/article-create.component';
import { ArticleAssignComponent } from './article-assign/article-assign.component';
import { ArticleEditComponent } from './article-edit/article-edit.component';
import { ArticleViewComponent } from './article-view/article-view.component';

import { SharedModule } from "app/shared/shared.module";

import { PaginationModule, ModalModule } from "ngx-bootstrap";   
import { TinymceModule } from 'angular2-tinymce';

@NgModule({
    imports: [
        ArticleListRoutingModule,
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        PaginationModule,
        ModalModule,
        TinymceModule.withConfig({
            plugins: [
                "advlist autolink lists link image charmap print preview anchor",
                "searchreplace visualblocks code fullscreen",
                "insertdatetime media table contextmenu paste imagetools"
            ],
            toolbar: "insertfile undo redo | styleselect"
            +"| bold italic | alignleft aligncenter alignright alignjustify"
            +"| bullist numlist outdent indent | link image"
            ,
            height: 200,
            baseURL: "./assets/tinymce",
            skin_url: "./assets/tinymce/skins/lightgray"
        }),
        SharedModule
    ],
    declarations: [
        ArticleListComponent,
        ArticleCreateComponent,
        ArticleAssignComponent,
        ArticleEditComponent,
        ArticleViewComponent
    ],
    providers: [ArticlesService],
    exports: [
        ArticleAssignComponent,
        ArticleViewComponent
    ]
})
export class ArticlesModule { }
