import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { EventCreateComponent } from './event-create/event-create.component';
import { EventEditComponent } from './event-edit/event-edit.component';
import { EventListComponent } from './event-list/event-list.component';
import { EventService } from './event.service';

import { ArticlesModule } from '../articles/articles.module';

import { ModalModule, PaginationModule, TabsModule, TimepickerModule } from 'ngx-bootstrap';
import { CalendarModule } from 'primeng/primeng';
import { SelectModule } from 'ng2-select';
import { EventDetailsComponent } from './event-details/event-details.component';
import { SharedModule } from "app/shared/shared.module";
import { VisualizationModule } from "app/visualizations/visualization.module";

@NgModule({
    imports: [
        ModalModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        ArticlesModule,
        PaginationModule,
        SelectModule,
        TabsModule,
        TimepickerModule,
        CalendarModule,
        SharedModule
    ],
    exports: [EventListComponent, EventDetailsComponent],
    declarations: [EventCreateComponent, EventEditComponent, EventListComponent, EventDetailsComponent],
    providers: [EventService],
})

export class EventModule { }
