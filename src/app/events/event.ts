export interface Event {
    title: string;
    short_description: string;
    long_description?: string;
    date: string;
    tags: Array<any>;
    articles: Array<any>;
    image_url?: string
}