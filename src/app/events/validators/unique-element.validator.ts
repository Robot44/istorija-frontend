import { FormControl } from '@angular/forms';

function unique (array: Array<any>) {

    return (control: FormControl) {
        return array.indexOf(control.value) === -1 ? null: {
            unique: {valid: false}
        }
    }
}