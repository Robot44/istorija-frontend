import { Component, OnInit, ViewChild } from '@angular/core';

import { Event } from "../event";

import { ArticleViewComponent } from "../../articles/article-view/article-view.component";

import { GlobalService } from "app/shared/global.service";

import { TabsetComponent, ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'event-details',
  templateUrl: './event-details.component.html',
  styleUrls: ['./event-details.component.scss']
})
export class EventDetailsComponent {
  @ViewChild(ModalDirective) modal : ModalDirective;
  @ViewChild(ArticleViewComponent) article: ArticleViewComponent;
  @ViewChild(TabsetComponent) staticTabs: TabsetComponent;
  private event:Event;
  private title:string = '';
  private showArticle:boolean = false;
  private url: string;

  constructor(private global: GlobalService) {
    this.url = global.getConfig().api;
  }

  showModal(event:Event) {
    this.event = event;
    this.showArticle = false;
    this.title = event.title;
    this.modal.show();
    this.selectTab(0);
  }

  selectTab(tabId: number) {
    if(this.staticTabs) {
      this.staticTabs.tabs[tabId].active = true;
    }
  }

  onHide() {
    this.event = null;
    this.title = '';
    this.showArticle = false;
    if(this.article){
      this.article.clear();
    }
  }

  onChange(value) {
    if(value != "0"){
      this.showArticle = true;
      this.article.loadArticle(value);
    }
  }
}
