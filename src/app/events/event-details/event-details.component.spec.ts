import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';

import { EventDetailsComponent } from './event-details.component';

import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { EventCreateComponent } from '../event-create/event-create.component';
import { EventEditComponent } from '../event-edit/event-edit.component';
import { EventListComponent } from '../event-list/event-list.component';
import { EventService } from '../event.service';

import { ArticlesModule } from '../../articles/articles.module';

import { ModalModule, PaginationModule, TabsModule, TimepickerModule } from 'ngx-bootstrap';
import { CalendarModule } from 'primeng/primeng';

import { SelectModule } from 'ng2-select';
import { Event } from "app/events/event";
import { GlobalService } from 'app/shared/global.service';
import { SharedModule } from "app/shared/shared.module";

describe('EventDetailsComponent', () => {
  let component: EventDetailsComponent;
  let fixture: ComponentFixture<EventDetailsComponent>;

  beforeEach(async(() => {
      TestBed.configureTestingModule({
      imports: [
          ModalModule.forRoot(),
          FormsModule,
          ReactiveFormsModule,
          CommonModule,
          ArticlesModule,
          PaginationModule.forRoot(),
          SelectModule,
          TabsModule.forRoot(),
          TimepickerModule.forRoot(),
          CalendarModule,
          SharedModule
      ],
      declarations: [EventCreateComponent, EventEditComponent, EventListComponent, EventDetailsComponent],
      providers: [EventService, GlobalService],
      })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventDetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load data successfully', async(() => {
    let event = {
      "id":65,
      "date":"1896-06-04 00:00",
      "title":"Penatibus luctus ut tempor dapibus nisl enim diam a, lorem felis lacus non mi accumsan est, dolor vitae dictum praesent tortor cubilia metus.",
      "short_description":"<4>Mi himenaeos egestas euismod nam purus mus primis donec semper, luctus quisque aenean montes dictum dui pellentesque nisi sagittis amet, morbi aliquet lorem eros varius curabitur adipiscing lacus.</4>",
      "long_description":"Maecenas ipsum blandit nisi urna fusce, lacus sapien conubia cras magnis, ultricies condimentum vestibulum mi. Nec fringilla ut augue dignissim montes hendrerit erat mi dictum malesuada eros, a sociosqu nullam facilisis integer quis convallis donec tempus",
      "articles":[
        {"id":"5e6649b3-3260-11e7-ab91-bc5ff4aa81a6","title":"Ullamcorper nostra malesuada u"}
      ],
      "tags":[
        {"id":1,"title":"even","description":"Lorem ipsum dolor sit amet consectetur adipiscing elit consequat interdum, non ullamcorper elementum auctor ac curabitur torquent in, platea euismod inceptos posuere arcu massa vivamus cursus.","color":"rgb(214, 70, 87)"}
      ]};
    component.showModal(event);
    fixture.whenStable().then(()=>{
      expect(component).toBeTruthy();
    });
  }));

});
