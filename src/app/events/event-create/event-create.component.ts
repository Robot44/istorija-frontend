import { Component, OnInit, Input, Output, ViewChild, EventEmitter, ElementRef } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';

import { ModalDirective } from 'ngx-bootstrap';
import { SelectComponent } from 'ng2-select';

import { FlashMessageService } from '../../shared/flash-message/flash-message.service';
import { TagService } from '../../tag/tag.service';

import { Event } from '../event';
import { EventService } from '../event.service';
import * as moment from 'moment';

@Component({
    selector: 'event-create',
    templateUrl: 'event-create.component.html',
    styleUrls: ['./event-create.component.scss']
})

export class EventCreateComponent{
    @Input() timelineId: number;
    @Output() renew = new EventEmitter<boolean>();
    @ViewChild(ModalDirective) modal : ModalDirective;
    @ViewChild('tagsSelected') select: SelectComponent;
    @ViewChild('imageInput') imageInput: ElementRef;

    public eventForm: FormGroup;
    private submitted: boolean = false;
    public date: AbstractControl;
    public title: AbstractControl;
    public description: AbstractControl;
    public longDescription: AbstractControl;    
    public tags: AbstractControl;
    public file: AbstractControl;
    private availableTags;
    private imageBase64: string;
    public imageUrl: string;
    private isLoading: boolean;

    constructor( 
        private flashMessageService: FlashMessageService,
        private eventService: EventService,
        private router: Router,
        private fb: FormBuilder,
        private tagService: TagService
        ) {
            this.createForm();
        }

    showModal(timelineId) {
        this.isLoading = false;
        this.reset();
        this.timelineId = timelineId;
        this.loadTags();
        this.modal.show();
    }

    loadTags() {
        this.tagService.getTags()
        .then(
            data => {
                this.availableTags = [];
                data.forEach(element => {
                    this.availableTags.push({id:element.id, text:element.title});
                });
                this.select.items = this.availableTags;
            },
            error => {
                this.flashMessageService.show('Failed to fetch available tags!', 'error');
            }
        );
    }
    
    onSubmit() {
        this.submitted = true;
        if(this.eventForm.valid) {
            this.isLoading = true;
            let event = <Event>{};
            event.title = this.eventForm.value.title;
            event.date = moment(this.date.value).format('YYYY-MM-DD HH:mm');
            event.short_description = this.eventForm.value.description;
            event.long_description = ""+this.eventForm.value.longDescription;
            event.tags = [];
            let tags = this.tags.value;
            if(Array.isArray(tags) && tags.length) {
                tags.forEach(element => {
                    event.tags.push(element.id);
                });
            }
            this.eventService.postEvent(this.timelineId, JSON.stringify(event))
            .then(
                 data => {
                    if(this.imageBase64) {
                        this.eventService.putImage(this.timelineId, data.id, this.imageBase64).then(data => {
                                this.handleSuccess('Event created successfully!');
                            },
                            error => {
                                this.handleFailure('Failed update image!');
                            }
                        );
                    } else { 
                        this.handleSuccess('Event created successfully!');
                    }
                },
                error => {
                    this.handleFailure('Failed to create event!');
                }
            );    
        }
    }

    onFileChange(event) {
        var allowedFileTypes = ["image/png", "image/jpeg"];
        var file:File = event.srcElement.files[0];
        var myReader:FileReader = new FileReader();
        this.imageBase64 = null;
        this.imageUrl = null;
        this.file.markAsDirty();
        this.file.markAsTouched();
        if(file.size >= 1024 * 1024) {
            this.file.setErrors({tooLarge: true});
        } else if(allowedFileTypes.indexOf(file.type) < 0) {
            this.file.setErrors({invalidType: true});
        } else {
            myReader.onloadend = (e) => {
                let image: string = myReader.result;
                this.imageBase64 = image.split(',')[1];
                this.imageUrl =  myReader.result;
            }
            myReader.readAsDataURL(file);
        }
    }

    reset() {
        this.eventForm.reset();
        this.submitted = false;
        this.date.setValue(new Date());
        this.title.setValue('');
        this.description.setValue('');
        this.longDescription.setValue('');
        this.removeImage();
    }

    createForm() {
        this.eventForm = this.fb.group({
            date: [new Date(), Validators.required],
            title: ['', Validators.compose([Validators.required, Validators.maxLength(40)])],
            description: ['', Validators.compose([Validators.required, Validators.maxLength(80)])],
            longDescription: ['', Validators.compose([Validators.maxLength(255)])],
            tags: [[]],
            file:[]
        });
        this.file = this.eventForm.controls['file'];
        this.date = this.eventForm.controls['date'];
        this.title = this.eventForm.controls['title'];
        this.description = this.eventForm.controls['description'];
        this.longDescription = this.eventForm.controls['longDescription'];
        this.tags = this.eventForm.controls['tags'];
    }

    handleSuccess(message?){
        if(message){
            this.flashMessageService.show(message,'success');
        }
        this.renew.emit(true);
        this.modal.hide();
    }

    handleFailure(message?){
        if(message){
            this.flashMessageService.show(message,'error');
        }
        this.renew.emit(true);
        this.modal.hide();
    }

    removeImage() {
        this.imageInput.nativeElement.value = null;
        this.imageBase64 = null;
        this.imageUrl = null;
    }
}