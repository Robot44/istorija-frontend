import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { EventCreateComponent } from "./event-create.component";

import { FlashMessageService } from '../../shared/flash-message/flash-message.service';
import { MockFlashMessageService } from '../../shared/mock/flash-message.service';
import { EventService } from "../event.service";
import { MockEventService } from "../mock/event.service";
import { TagService } from '../../tag/tag.service';
import { MockTagService } from '../../tag/mocks/tag.service';
import { GlobalService } from 'app/shared/global.service';

import { SelectModule } from 'ng2-select';
import { TabsModule, Ng2BootstrapModule, PaginationModule, BsDropdownModule, ModalModule } from 'ngx-bootstrap';

import { Component, OnInit, Input, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';
import { Router, Params } from '@angular/router';

import { ModalDirective } from 'ngx-bootstrap';
import { SelectComponent } from 'ng2-select';
import {CalendarModule} from 'primeng/primeng';

import { Event } from '../event';
import * as moment from 'moment';

import { Observable } from 'rxjs/Rx';

describe('EventCreateComponent', () => {
  let component: EventCreateComponent;
  let fixture: ComponentFixture<EventCreateComponent>;
  let eventService: EventService;

  let mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventCreateComponent ],
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SelectModule,
        PaginationModule.forRoot(),
        ModalModule.forRoot(),
        PaginationModule.forRoot(),
        FormsModule,
        Ng2BootstrapModule.forRoot(),
        BsDropdownModule.forRoot(),
        ModalModule.forRoot(),
        CalendarModule
      ],
      providers: [
        {provide: EventService, useClass: MockEventService},
        {provide: FlashMessageService, useClass: MockFlashMessageService},
        {provide: Router, useValue: mockRouter},
        {provide: TagService, useClass: MockTagService},
        GlobalService
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventCreateComponent);
    component = fixture.componentInstance;
    eventService = TestBed.get(EventService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have correct payload for service', async(() => {
    spyOn(eventService, 'postEvent').and.callThrough();
    component.showModal(1);
    fixture.detectChanges();
    component.title.setValue('something else');
    component.description.setValue('something else too');
    component.longDescription.setValue('longlong');
    component.date.setValue("1212-12-21 10:10");
    component.tags.setValue([
        {id:2, title:"first", description:"first descr"},
        {id:3, title:"first", description:"first descr"}
    ]);
    component.onSubmit();
    fixture.whenStable().then(()=>{
        expect(eventService.postEvent).toHaveBeenCalledWith(1, JSON.stringify({
            title:"something else",
            date: "1212-12-21 10:10",
            short_description:"something else too",
            long_description:"longlong",
            tags: [2,3]
        }));
    });
  }));

  it('form should be valid before changes', () => {
    fixture.detectChanges();
    fixture.whenStable().then(()=>{
      expect(component.eventForm.valid).toBeTruthy();
    });
  });

  it('form should be valid after changes', async() => {
    fixture.detectChanges();
    component.title.setValue('something else');
    component.description.setValue('something else too');
    fixture.whenStable().then(()=>{
      expect(component.eventForm.valid).toBeTruthy();
    });
  });
});
