import { Injectable } from '@angular/core';
import { JwtHttp } from 'angular2-jwt-refresh';
import { URLSearchParams, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { GlobalService } from "app/shared/global.service";

@Injectable()
export class EventService {

    constructor(private authHttp: JwtHttp, private global: GlobalService) {
        if(this.global) {
            this.baseUrl = this.global.getConfig().baseUrl + 'timelines/';
        }
    }

    private baseUrl:string;

    putImage(timelineId, eventId, imageBase64) {
        let headers = new Headers();
        headers.append('Content-Type', 'application/json')
        let json = { imageFile: imageBase64 };
        return this.authHttp.put(this.baseUrl + timelineId + '/events/' + eventId + '/image', json)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    deleteImage(timelineId, eventId) {
        return this.authHttp.delete(this.baseUrl + timelineId + '/events/' + eventId + '/image')
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getPage(timelineId, page) {
        let body = new URLSearchParams();
        body.set('_page', page.toString());
        return this.authHttp.get(this.baseUrl + timelineId + '/events', { search : body})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getEvent(timelineId, eventId) {
        return this.authHttp.get(this.baseUrl + timelineId + '/events/' + eventId)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    postEvent(timelineId, eventJson) {
        let myHeader = new Headers();
        myHeader.append('Content-Type', 'application/json');

        return this.authHttp.post(this.baseUrl + timelineId + '/events', eventJson, { headers: myHeader })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    deleteEvent(timelineId, eventId) {
        return this.authHttp.delete(this.baseUrl + timelineId + '/events/' + eventId)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    putEvent(timelineId , eventId, eventJson) {
        let myHeader = new Headers();
        myHeader.append('Content-Type', 'application/json');
        
        return this.authHttp.put(this.baseUrl + timelineId + '/events/' + eventId, eventJson, { headers: myHeader })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}