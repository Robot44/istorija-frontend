import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';

import { EventListComponent } from "./event-list.component";
import { FlashMessageService } from '../../shared/flash-message/flash-message.service';
import { MockFlashMessageService } from '../../shared/mock/flash-message.service';
import { EventService } from "../event.service";
import { MockEventService } from "../mock/event.service";

import { PaginationModule, ModalModule } from 'ngx-bootstrap';
describe('EventListComponent', () => {
  let component: EventListComponent;
  let fixture: ComponentFixture<EventListComponent>;
  let eventService: EventService;

  let mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EventListComponent ],
       imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        PaginationModule.forRoot(),
        ModalModule.forRoot()
      ],
      providers: [
        {provide: EventService, useClass: MockEventService},
        {provide: FlashMessageService, useClass: MockFlashMessageService},
        {provide: Router, useValue: mockRouter}
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EventListComponent);
    component = fixture.componentInstance;
    eventService = TestBed.get(EventService);
    component.timelineId = 1;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  
  it('should have two elements', async(() => {
    fixture.whenStable().then(() => {
      fixture.detectChanges();       
      expect(fixture.nativeElement.querySelectorAll("tbody tr").length).toBe(2);
    });
  }));

  
  it('should call service delete on click', fakeAsync(() => {
    spyOn(eventService, 'deleteEvent').and.callThrough();
    component.ngOnInit();
    tick();
    fixture.detectChanges();        
    let element = fixture.nativeElement.querySelector(".remove");
    element.click();
    tick();
    fixture.detectChanges();    
    expect(eventService.deleteEvent).toHaveBeenCalled();
  })); 
});
    