import { Component, OnInit, ViewChild, Input } from '@angular/core';

import { FlashMessageService } from '../../shared/flash-message/flash-message.service';

import { Event } from '../event';
import { EventService } from '../event.service'

@Component({
    selector: 'event-list',
    templateUrl: 'event-list.component.html'
})
export class EventListComponent implements OnInit {
    constructor( 
        private flashMessageService: FlashMessageService,
        private eventService: EventService
    ) { }

    public totalItems: number = 0;
    public currentPage: number = 1;
    public itemsPerPage: number = 0;
    public events: Array<Event> = [];
    @Input() timelineId: number = null;
    public isLoading: boolean;

    ngOnInit() {
        this.isLoading = true;
        this.getPage(this.currentPage);
    }

    public pageChanged(event: any): void {
        this.getPage(event.page);
    }

    deleteEvent(event) {
        let target = event.currentTarget;
        let id = target.attributes.id.value;
        this.eventService.deleteEvent(this.timelineId, id)
        .then(
            data => {
                this.flashMessageService.show('Event removed successfully!', 'success');
                this.refresh();
            },
            error => {
                this.flashMessageService.show('Failed to remove event!', 'error');
                this.refresh();
            }
        );
    }

    private getPage(page) {
        this.isLoading = true;
        this.eventService.getPage(this.timelineId, page)
        .then(
            data => {
                this.events = data.events;
                this.totalItems = data.total;
                this.itemsPerPage = data.limit;
                this.isLoading = false;
            },
            error => {
                this.flashMessageService.show('Failed to load page!', 'error');
            }
        );

        return page;
    }

    private refresh() {
        this.getPage(this.currentPage);
    }
}