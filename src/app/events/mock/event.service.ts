import { EventService } from '../event.service';
import { GlobalService } from 'app/shared/global.service';

export class MockEventService extends EventService{
    constructor() {
        super(null, null);
    }

    getPage(page) {
        return Promise.resolve({
            events: [
                {id: "first", title: "title", short_description: "short descr"},
                {id: "second", title: "title", short_description: "short descr"}
            ],
            total: 2,
            limit: 5
        });
    }

    deleteEvent(timelineId, id) {
        return Promise.resolve({id: "first", title: "title", short_description: "short descr"});
    }

    getEvent(id) {
        return Promise.resolve({
            id: 1,
            title: "title",
            short_description: "short",
            long_description: "long",
            date:"2089-01-06 00:00",
            tags: [{id:5, title: "title", description:"desc"}]
        });
    }

    putEvent(id) {
        return Promise.resolve({
            id: 1,
            title: "title",
            short_description: "short",
            long_description: "long",
            date:"2089-01-06 00:00",
            tags: [{id:5, title: "title", description:"desc"}]
        });
    }

    postEvent(timelineId, eventJson)  {
        return Promise.resolve({
            id: 1,
            title: "title",
            short_description: "short",
            long_description: "long",
            date:"2089-01-06 00:00",
            tags: [{id:5, title: "title", description:"desc"}]
        });
    }
}