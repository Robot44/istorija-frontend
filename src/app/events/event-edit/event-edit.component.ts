import { Component, OnInit, Input, ViewChild, Output, EventEmitter, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';
import { Router, Params } from '@angular/router';

import { ModalDirective } from 'ngx-bootstrap';
import { SelectComponent } from 'ng2-select';

import { FlashMessageService } from '../../shared/flash-message/flash-message.service';
import { TagService } from '../../tag/tag.service';
import { GlobalService } from "app/shared/global.service";

import { Event } from '../event';
import { EventService } from '../event.service';
import * as moment from 'moment';

@Component({
    selector: 'event-edit',
    templateUrl: 'event-edit.component.html',
    styleUrls: ['./event-edit.component.scss']
})
export class EventEditComponent { 
    @Output() renew = new EventEmitter<boolean>();
    @ViewChild(ModalDirective) modal : ModalDirective;
    @ViewChild('tagsSelected') select: SelectComponent;
    @ViewChild('imageInput') imageInput: ElementRef;

    public eventForm: FormGroup;
    public submitted = false;
    public date: AbstractControl;
    public title: AbstractControl;
    public description: AbstractControl;
    public longDescription: AbstractControl;
    public tags: AbstractControl;
    public file: AbstractControl;
    public availableTags;
    public timelineId: number;
    public eventId: number;
    public imageBase64: string;
    public imageUrl: string;
    public isLoading: boolean;
    public deleteImage: boolean;

    constructor( 
        private flashMessageService: FlashMessageService,
        private eventService: EventService,
        private router: Router,
        private fb: FormBuilder,
        private tagService: TagService,
        private global: GlobalService
    ) {
        this.createForm();
    }

    showModal(timelineId, eventId) {
        this.deleteImage = false;
        this.imageBase64 = null;
        this.imageUrl = null;
        this.eventForm.reset();
        this.timelineId = timelineId;
        this.eventId = eventId;
        this.getEvent();
        this.submitted = false;
        this.imageInput.nativeElement.value = null;
        this.modal.show();
    }

    onFileChange(event) {
        var allowedFileTypes = ["image/png", "image/jpeg"];
        var file:File = event.srcElement.files[0];
        var myReader:FileReader = new FileReader();
        this.imageBase64 = null;
        this.imageUrl = null;
        this.file.markAsDirty();
        this.file.markAsTouched();
        if(file.size >= 1024 * 1024) {
            this.file.setErrors({tooLarge: true});
        } else if(allowedFileTypes.indexOf(file.type) < 0) {
            this.file.setErrors({invalidType: true});
        } else {
            myReader.onloadend = (e) => {
                let image: string = myReader.result;
                this.imageBase64 = image.split(',')[1];
                this.imageUrl =  myReader.result;
            }
            myReader.readAsDataURL(file);
        }    
    }

    clearFile() {
        this.imageInput.nativeElement.value = null;
        this.imageUrl = null;
        this.imageBase64 = null;
        this.file.reset();
    }

    onSubmit() {
        this.submitted = true;
        if(this.eventForm.valid) {
            this.isLoading = true;
            let event = <Event>{};
            event.title = this.eventForm.value.title;
            event.date = moment(this.date.value).format('YYYY-MM-DD HH:mm').toString();
            event.short_description = this.eventForm.value.description;
            event.long_description = ""+this.eventForm.value.longDescription;
            event.tags = [];
            let tags = this.tags.value;
            tags.forEach(element => {
                event.tags.push(element.id);
            });
            this.eventService.putEvent(this.timelineId, this.eventId, JSON.stringify(event))
            .then( () => {
                this.flashMessageService.show('Event updated successfully!', 'success');
                if(this.imageBase64) {
                    this.eventService.putImage(this.timelineId, this.eventId, this.imageBase64)
                    .catch( error => {
                        this.flashMessageService.show('Failed update image!', 'error');
                    });
                } else if(this.deleteImage) { 
                    this.eventService.deleteImage(this.timelineId, this.eventId)
                    .catch(error => {
                        this.flashMessageService.show('Failed remove image!', 'error');
                    });
                }
                this.renew.emit(true);
            }).catch( error => {
                this.flashMessageService.show('Failed to update event!', 'error');
            }).then( () => {
                this.modal.hide();
            });
        }
    }

    getEvent() {
        this.isLoading = true;
        this.tagService.getTags()
        .then(
            data => {
                this.availableTags = [];
                data.forEach(element => {
                    this.availableTags.push({id:element.id, text:element.title});
                });
                this.select.items = this.availableTags;
            }, 
            error =>{
                this.flashMessageService.show('Failed to load events tags!', 'alert-warning');
            } 
        );
        this.eventService.getEvent(this.timelineId, this.eventId)
        .then(
            data => {
                if(data.image_url) {
                    this.imageUrl = this.global.getConfig().api + data.image_url;
                }
                this.date.setValue(new Date(data.date));
                this.title.setValue(data.title);
                this.description.setValue(data.short_description);
                this.longDescription.setValue(data.long_description);
                let tags = [];
                if(data.tags) {
                    data.tags.forEach(element => {
                        tags.push({id:element.id, text:element.title});
                    });
                }
                this.tags.setValue(tags);
                this.isLoading = false;
            },
            error => {
                this.flashMessageService.show('Failed to load event!', 'alert-warning');
                this.modal.hide();
            }
        );
    }

    createForm() {
        this.eventForm = this.fb.group({
            date:['', Validators.required],
            title: ['', Validators.compose([Validators.required, Validators.maxLength(40)])],
            description: ['', Validators.compose([Validators.required, Validators.maxLength(80)])],
            longDescription: ['', Validators.compose([Validators.maxLength(255)])],
            tags:[[]],
            file:[]
        });
        this.date = this.eventForm.controls['date'];
        this.title = this.eventForm.controls['title'];
        this.description = this.eventForm.controls['description'];
        this.longDescription = this.eventForm.controls['longDescription'];
        this.tags = this.eventForm.controls['tags'];
        this.file = this.eventForm.controls['file'];
    }

    removeImage() {
        this.imageInput.nativeElement.value = null;
        this.imageBase64 = null;
        this.imageUrl = null;
        this.deleteImage = true;
    }
}