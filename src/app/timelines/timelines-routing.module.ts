import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TimelineListComponent } from './timeline-list/timeline-list.component';
import { TimelineCreateComponent } from './timeline-create/timeline-create.component';
import { TimelineEditComponent } from './timeline-edit/timeline-edit.component';

const routes: Routes = [
  { path: '', component: TimelineListComponent },
  { path: 'edit/:id', component: TimelineEditComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TimelinesRoutingModule { }