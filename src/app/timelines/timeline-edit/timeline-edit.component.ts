import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, ActivatedRoute, Params  } from '@angular/router';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';

import { FlashMessageService } from '../../shared/flash-message/flash-message.service';

import { TabsetComponent } from 'ngx-bootstrap';

import { TimelineService } from '../timeline.service';
import { Timeline } from '../timeline';

@Component({
    selector: 'timeline-edit',
    templateUrl: './timeline-edit.component.html'
})
export class TimelineEditComponent implements OnInit {
    @ViewChild(TabsetComponent) staticTabs: TabsetComponent;
    public id: Number; 
    public title: AbstractControl;
    public description: AbstractControl;
    public color: AbstractControl;

    public timelineForm: FormGroup;
    public submitted = false;
    public isLoading = true;


    constructor( 
        private flashMessageService: FlashMessageService,
        private router: Router,
        private route: ActivatedRoute,
        private timelineService: TimelineService,
        private fb: FormBuilder
    ) { 
        this.createForm();
    }

    ngOnInit() {
        this.route.params.subscribe(
            params => {
                this.id = +params['id'];
                this.loadTimeline(this.id);
            },
            error => {
                this.flashMessageService.show('Failed to load timeline!', 'error');
            }
        );
    }

    loadTimeline(id) {
        this.timelineService.getTimeline(id)
        .then(
            data => {
                this.title.setValue(data.title);
                this.description.setValue(data.description);
                if(data.color) {
                    this.color.setValue(data.color);
                } else {
                    this.color.setValue('rgb(112, 96, 188)');
                }
                this.isLoading = false;
            },
            error => {
                this.flashMessageService.show('Failed to load timeline!', 'error');
                this.refresh();
            }
        );
    }

    onSubmit() {
        this.submitted = true;
        let model = new Timeline('','');
        if (this.timelineForm.valid) {
            model.title = this.title.value;
            model.description = this.description.value;
            if(this.color.value == ''){
                model.color = null;
            } else {
                model.color = this.color.value;
            }
            this.timelineService.putTimeline(this.id, JSON.stringify(model))
            .then(
                data => {
                    this.flashMessageService.show('Timeline updated successfully!', 'success');
                    this.refresh();
                },
                error => {
                    this.flashMessageService.show('Failed to update timeline!', 'error');
                    this.refresh();
                }
            );
        }
    }

    createForm() {
        this.timelineForm = this.fb.group({
            title: ['', Validators.compose([Validators.required, Validators.maxLength(40)])],
            description: ['', Validators.compose([Validators.required, Validators.maxLength(255)])],
            color: [''],
        });
        
        this.title = this.timelineForm.controls['title'];
        this.description = this.timelineForm.controls['description'];
        this.color = this.timelineForm.controls['color'];
    }

    private refresh() {
        this.router.navigate(['/timelines']);
    }

    get currentColor() {
        return this.color.value;
    }

    onColorPickerChange(colorCode) {
        this.color.setValue(colorCode);
        this.color.markAsDirty();
        this.color.markAsTouched();
    }
}