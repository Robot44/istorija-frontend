import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute  } from '@angular/router';

import { TimelineService } from '../timeline.service';
import { MockTimelineService } from '../mock/timeline.service';
import { FlashMessageService } from '../../shared/flash-message/flash-message.service';
import { MockFlashMessageService } from '../../shared/mock/flash-message.service';

import { Observable } from 'rxjs/Rx';
import { ColorPickerModule } from 'ngx-color-picker';

import { TimelineEditComponent } from './timeline-edit.component';
import { SharedModule } from "app/shared/shared.module";

describe('TimelineEditComponent', () => {
  let component: TimelineEditComponent;
  let fixture: ComponentFixture<TimelineEditComponent>;
  let timelineService: TimelineService;

  let mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimelineEditComponent ],
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ColorPickerModule,
        SharedModule
      ],
      providers: [
        {provide: TimelineService, useClass: MockTimelineService},
        {provide: FlashMessageService, useClass: MockFlashMessageService},
        {provide: Router, useValue: mockRouter},
        {
          provide: ActivatedRoute,
          useValue: {
            params: Observable.of({id: 1})
          }
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineEditComponent);
    component = fixture.componentInstance;
    timelineService = TestBed.get(TimelineService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load timeline data on showModal call', fakeAsync(() => {
    spyOn(timelineService, 'getTimeline').and.callThrough();
    component.ngOnInit();
    tick(1000);
    fixture.detectChanges();
    tick();
    tick();
      expect(timelineService.getTimeline).toHaveBeenCalledWith(1);
      expect(fixture.nativeElement.querySelector("#title").value).toBe("first");
      expect(fixture.nativeElement.querySelector("#description").value).toBe("first descr");
  }));

  it('should have correct payload for service', fakeAsync(() => {
    spyOn(timelineService, 'getTimeline').and.callThrough();
    spyOn(timelineService, 'putTimeline').and.callThrough();
    component.ngOnInit();
    tick(1000);
    fixture.detectChanges();
    tick();
    tick();
    component.title.setValue('something else');
    component.description.setValue('something else too');
    component.onSubmit();
    tick();
    expect(timelineService.putTimeline).toHaveBeenCalledWith(1, JSON.stringify({title:"something else", description:"something else too", color:"rgb(66,134,244)"}));
  }));

  it('form should be valid before changes', () => {
    fixture.detectChanges();
    fixture.whenStable().then(()=>{
      expect(component.timelineForm.valid).toBeTruthy();
    });
  });

  it('form should be valid after changes', async() => {
    fixture.detectChanges();
    fixture.nativeElement.querySelector
    component.title.setValue('something else');
    component.description.setValue('something else too');
    fixture.whenStable().then(()=>{
      expect(component.timelineForm.valid).toBeTruthy();
    });
  });
});
