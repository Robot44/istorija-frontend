import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { TimelineListComponent } from './timeline-list.component';

import { FlashMessageService } from '../../shared/flash-message/flash-message.service';
import { MockFlashMessageService } from '../../shared/mock/flash-message.service';
import { TimelineService } from "../timeline.service";
import { MockTimelineService } from "../mock/timeline.service";

import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';

import { ModalModule } from 'ng2-bootstrap/modal';
import { PaginationModule } from 'ng2-bootstrap/pagination';
import { SharedModule } from "app/shared/shared.module";

describe('TimelineListComponent', () => {
  let component: TimelineListComponent;
  let fixture: ComponentFixture<TimelineListComponent>;
  let timelineService: TimelineService;

  let mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimelineListComponent ],
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        PaginationModule.forRoot(),
        ModalModule.forRoot(),
        SharedModule
      ],
      providers: [
        {provide: TimelineService, useClass: MockTimelineService},
        {provide: FlashMessageService, useClass: MockFlashMessageService},
        {provide: Router, useValue: mockRouter },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineListComponent);
    component = fixture.componentInstance;
    timelineService = TestBed.get(TimelineService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have two elements in list', fakeAsync(() => {
    component.ngOnInit();
    tick();
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelectorAll("tbody tr").length).toBe(2);
  }));
  
  it('should call service delete on click', fakeAsync(() => {
    spyOn(timelineService, 'deleteTimeline').and.callThrough();
    component.ngOnInit();
    tick();
    fixture.detectChanges();
    let element =  fixture.nativeElement.querySelector(".remove");
    element.click();
    tick();
    expect(timelineService.deleteTimeline).toHaveBeenCalledWith("1");
  }));
});
