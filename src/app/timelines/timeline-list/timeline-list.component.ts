import { Component, OnInit } from '@angular/core';
import { URLSearchParams } from '@angular/http';
import { Router } from '@angular/router';

import { FlashMessageService } from '../../shared/flash-message/flash-message.service';

import { TimelineService } from '../timeline.service';

@Component({
    selector: 'timeline-list',
    templateUrl: 'timeline-list.component.html'
})
export class TimelineListComponent implements OnInit {
    public totalItems: number = 0;
    public currentPage: number = 1;
    public itemsPerPage: number = 0;
    public items: Array<any> = [];
    public isLoading: boolean;

    constructor( 
        private flashMessageService: FlashMessageService,
        private router: Router,
        private timelineService: TimelineService
      ) { }
    
    ngOnInit() {
        this.isLoading = true;
        this.getPage(this.currentPage);
    }

    public getPage(page){
        this.isLoading = true;
        this.timelineService.getPage(page)
        .then(
            data => {
                this.items = data.timelines;
                this.currentPage = data.page;
                this.totalItems = data.totalTimelines;
                this.itemsPerPage = data.limit;
                this.isLoading = false;
            },
            error => {
                this.flashMessageService.show('Failed to load page!', 'error');
            }
        );

        return page;
    }

    deleteTimeline(event) {
        let target = event.currentTarget;
        let id = target.attributes.id.value;
        this.timelineService.deleteTimeline(id)
        .then(
            data => {
                this.flashMessageService.show('Timeline removed successfully!', 'success');
                this.refresh();
            },
            error => {
                this.flashMessageService.show('Failed to remove timeline!', 'error');
                this.refresh();
            }
        );
    }

    refresh() {
        this.getPage(this.currentPage);
        this.router.navigate(['/timelines']);
    }

    private pageChanged(event: any): void {
        this.getPage(event.page);
    }
}