import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule, DatePipe } from '@angular/common';

import { PaginationModule, ModalModule, TabsModule } from 'ngx-bootstrap';

import { TimelineListComponent } from './timeline-list/timeline-list.component';
import { TimelinesRoutingModule} from './timelines-routing.module';
import { TimelineCreateComponent } from './timeline-create/timeline-create.component';
import { TimelineService } from './timeline.service';
import { TimelineEditComponent } from './timeline-edit/timeline-edit.component';

import { EventModule } from '../events/event.module';
import { ArticlesModule } from '../articles/articles.module';
import { SharedModule } from "../shared/shared.module";
import { ColorPickerModule } from "ngx-color-picker";

@NgModule({
    imports: [
        TimelinesRoutingModule,
        CommonModule,
        PaginationModule,
        ModalModule,
        FormsModule,
        ReactiveFormsModule,
        ArticlesModule,
        EventModule,
        SharedModule,
        ColorPickerModule,
        TabsModule
    ],
    declarations: [
        TimelineListComponent,
        TimelineCreateComponent,
        TimelineEditComponent,
    ],
    providers: [
        TimelineService,
        DatePipe,
    ]
})
export class TimelinesModule { }
