import { TimelineService } from "../timeline.service";

export class MockTimelineService extends TimelineService {
    constructor() {
        super(null, null);
    }

    getPage(page) {
        return Promise.resolve({
            timelines: [
                {id:1, title:"first", description:"first descr", color:"rgb(66,134,244)"},
                {id:2, title:"second", description:"second descr", color:"rgb(66,134,244)"}
            ],
            page: 1,
            totalTimelines:2,
            limit: 5
        });
    }
    getTimeline(id) {
        return Promise.resolve({id:1, title:"first", description:"first descr", color:"rgb(66,134,244)"});
    }

    getTimelines() {
        return Promise.resolve([
                {id:1, title:"first", description:"first descr", color:"rgb(66,134,244)"},
                {id:2, title:"second", description:"second descr", color:"rgb(66,134,244)"},
                {id:3, title:"third", description:"third descr", color:"rgb(66,134,244)"}
            ]);
    }

    putTimeline(timelineId, timelineJson) {
      return Promise.resolve({id:1, title:"first", description:"first descr", color:"rgb(66,134,244)"})
    }

    postTimeline(timelineJson) {
      return Promise.resolve({id:1, title:"first", description:"first descr", color:"rgb(66,134,244)"})
    }

    deleteTimeline(id) {
        return Promise.resolve({id:1, title:"first", description:"first descr", color:"rgb(66,134,244)"});
    }
}