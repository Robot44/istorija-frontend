import { Injectable } from '@angular/core';
import { JwtHttp } from 'angular2-jwt-refresh';
import { URLSearchParams, Headers } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { GlobalService } from "app/shared/global.service";

@Injectable()
export class TimelineService {

    constructor(public authHttp: JwtHttp, private global: GlobalService) { 
        if(global) {
            this.baseUrl = this.global.getConfig().baseUrl + 'timelines'
        }
    }

    private baseUrl: string;

    getPage(page) {
        let body = new URLSearchParams();
        body.set('_page', page.toString());
        return this.authHttp.get(this.baseUrl, { search : body})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getTimeline(timelineId) {
        return this.authHttp.get(this.baseUrl + '/' + timelineId)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getTimelines() {
        return this.authHttp.get(this.baseUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    postTimeline(timelineJson) {
        let myHeader = new Headers();
        myHeader.append('Content-Type', 'application/json');

        return this.authHttp.post(this.baseUrl, timelineJson, { headers: myHeader })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    deleteTimeline(timelineId) {
        return this.authHttp.delete(this.baseUrl + '/' + timelineId)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    putTimeline(timelineId ,timelineJson) {
        let myHeader = new Headers();
        myHeader.append('Content-Type', 'application/json');
        return this.authHttp.put(this.baseUrl + '/' + timelineId, timelineJson, { headers: myHeader })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}