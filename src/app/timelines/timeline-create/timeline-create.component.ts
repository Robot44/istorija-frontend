import { Component, OnInit, ViewChild, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Headers } from '@angular/http';

import { FlashMessageService } from '../../shared/flash-message/flash-message.service';

import { Timeline } from '../timeline';
import { TimelineService } from '../timeline.service';

import { ModalDirective } from "ngx-bootstrap";

@Component({
    selector: 'timeline-create',
    templateUrl: './timeline-create.component.html'
})
export class TimelineCreateComponent {
    public title: AbstractControl;
    public description: AbstractControl;
    public color: AbstractControl;

    public timelineForm: FormGroup;
    public submitted = false;
    public isLoading = false;

    @Output() renew = new EventEmitter<boolean>();
    @ViewChild(ModalDirective) modal : ModalDirective;

    constructor( 
        private flashMessageService: FlashMessageService,
        private router: Router,
        private timelineService: TimelineService,
        private fb: FormBuilder
    ) {
        this.createForm();
    }

    showModal() {
        this.isLoading = false;
        this.submitted = false;
        this.reset();
        this.modal.show();
    }

    reset() {
        this.timelineForm.reset();
        this.description.setValue('');
        this.title.setValue('');
        this.color.setValue('rgb(112, 96, 188)');
    }

    onSubmit() {
        this.submitted = true;
        let model = new Timeline('','');
        if (this.timelineForm.valid) {
            this.isLoading = true;
            model.title = this.title.value;
            model.description = this.description.value;
            if(this.color.value == ''){
                model.color = null;
            } else {
                model.color = this.color.value;
            }

            this.timelineService.postTimeline(JSON.stringify(model))
            .then(
                data => {
                    this.flashMessageService.show('Timeline created successfully!', 'success');
                    this.renew.emit(true);
                    this.modal.hide();
                },
                error => {
                    this.flashMessageService.show('Failed to create timeline!', 'error');
                    this.modal.hide();
                }
            );
        }
    }

    createForm() {
        this.timelineForm = this.fb.group({
            title: ['', Validators.compose([Validators.required, Validators.maxLength(40)])],
            description: ['', Validators.compose([Validators.required, Validators.maxLength(255)])],
            color: ['rgb(112, 96, 188)']
        });
        
        this.title = this.timelineForm.controls['title'];
        this.description = this.timelineForm.controls['description'];
        this.color = this.timelineForm.controls['color'];
    }

    get currentColor(): string {
        return this.color.value;
    }

    onColorPickerChange(colorCode: string): void {
        this.color.setValue(colorCode);
        this.color.markAsDirty();
        this.color.markAsTouched();
    }
}