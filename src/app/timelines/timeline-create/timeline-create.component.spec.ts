import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router, ActivatedRoute  } from '@angular/router';

import { ModalModule, PaginationModule } from 'ngx-bootstrap';

import { TimelineService } from '../timeline.service';
import { MockTimelineService } from '../mock/timeline.service';
import { FlashMessageService } from '../../shared/flash-message/flash-message.service';
import { MockFlashMessageService } from '../../shared/mock/flash-message.service';

import { Observable } from 'rxjs/Rx';
import { ColorPickerModule } from 'ngx-color-picker';

import { TimelineCreateComponent } from './timeline-create.component';
import { SharedModule } from "app/shared/shared.module";

describe('TimelineCreateComponent', () => {
  let component: TimelineCreateComponent;
  let fixture: ComponentFixture<TimelineCreateComponent>;
  let timelineService: TimelineService;

  let mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TimelineCreateComponent ],
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ColorPickerModule,
        SharedModule,
        ModalModule.forRoot()
      ],
      providers: [
        {provide: TimelineService, useClass: MockTimelineService},
        {provide: FlashMessageService, useClass: MockFlashMessageService},
        {provide: Router, useValue: mockRouter},
        {
          provide: ActivatedRoute,
          useValue: {
            params: Observable.of({id: 1})
          }
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TimelineCreateComponent);
    component = fixture.componentInstance;
    timelineService = TestBed.get(TimelineService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have correct payload for service', async(() => {
    spyOn(timelineService, 'postTimeline').and.callThrough();
    fixture.detectChanges();
    component.title.setValue('something else');
    component.description.setValue('something else too');
    let element =  fixture.nativeElement.querySelector(".btn.btn-primary");
    component.color.setValue("rgb(66,134,24)");
    element.click();
    fixture.whenStable().then(()=>{
      expect(timelineService.postTimeline).toHaveBeenCalledWith(JSON.stringify({title:"something else", description:"something else too", color:"rgb(66,134,24)"}));
    });
  }));

  it('form should be invalid before changes', () => {
    fixture.detectChanges();
    fixture.whenStable().then(()=>{
      expect(component.timelineForm.valid).toBeFalsy();
    });
  });

  it('form should be valid after changes', async() => {
    fixture.detectChanges();
    fixture.nativeElement.querySelector
    component.title.setValue('something else');
    component.description.setValue('something else too');
    fixture.whenStable().then(()=>{
      expect(component.timelineForm.valid).toBeTruthy();
    });
  });
});
