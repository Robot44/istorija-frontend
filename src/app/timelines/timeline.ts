export class Timeline {
    constructor(
        public title: string,
        public description: string,
        public events: Array<any> = undefined,
        public color: any = undefined
    ) {}
}