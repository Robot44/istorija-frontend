export interface Tag {
    title: string,
    description: string,
    color: any
}