import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';

import { Tag } from '../tag';
import { TagService } from '../tag.service';

import { FlashMessageService } from '../../shared/flash-message/flash-message.service';;

import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'tag-edit',
  templateUrl: './tag-edit.component.html',
  styleUrls: ['./tag-edit.component.scss']
})
export class TagEditComponent {
    @Output() renew = new EventEmitter<boolean>();
    @ViewChild(ModalDirective) modal : ModalDirective;

    public title: AbstractControl;
    public description: AbstractControl;
    public color: AbstractControl;
    public tagId: number = null;
    public isLoading:boolean = true;
    public tagForm: FormGroup;
    public submitted: boolean = false;

    constructor(
        private fb: FormBuilder,
        private tagService: TagService,
        private flashMessageService: FlashMessageService,
        private sanitize: DomSanitizer
    ) {
        this.createForm();
    }

    showModal(tagId) {
        this.isLoading = true;
        this.submitted = false;
        this.tagForm.reset();
        this.tagId = tagId;
        this.tagService.getTag(tagId)
        .then(
            data => {
                this.title.setValue(data.title);
                this.description.setValue(data.description);
                this.color.setValue(data.color);
                this.isLoading = false;
            },
            error => {
                this.flashMessageService.show('Failed to load tag!', 'error');
            }
        );
        this.modal.show();
    }

    onSubmit() {
        this.submitted = true;
        let model = <Tag>{};
        if (this.tagForm.valid) {
            model.title = this.title.value;
            model.description = this.description.value;
            model.color = this.color.value;
            this.isLoading = true;
            this.tagService.putTag(this.tagId, JSON.stringify(model))
            .then(
                () => {
                    this.flashMessageService.show('Tag updated successfully!', 'success');
                    this.modal.hide();
                    this.renew.emit(true);
                }
            )
            .catch(
                () => this.flashMessageService.show('Failed to create tag!', 'error')
            );
        }
    }

    createForm() {
        this.tagForm = this.fb.group({
            title: ['', Validators.compose([Validators.required, Validators.maxLength(40)])],
            description: ['', Validators.compose([Validators.maxLength(255)])],
            color: ['#4286f4', Validators.required]
        });
        
        this.title = this.tagForm.controls['title'];
        this.description = this.tagForm.controls['description'];
        this.color = this.tagForm.controls['color'];
    }

    get currentColor() {
        return this.color.value;
    }

    onColorPickerChange(colorCode) {
        this.color.setValue(colorCode);
        this.color.markAsDirty();
        this.color.markAsTouched();
    }
}
