import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ModalModule, PaginationModule } from 'ngx-bootstrap';

import { TagService } from '../tag.service';
import { MockTagService } from '../mocks/tag.service';
import { FlashMessageService } from '../../shared/flash-message/flash-message.service';
import { MockFlashMessageService } from '../../shared/mock/flash-message.service';

import { Observable } from 'rxjs/Rx';

import { TagEditComponent } from './tag-edit.component';
import { SharedModule } from "app/shared/shared.module";
import { ColorPickerModule } from "ngx-color-picker";

describe('TagEditComponent', () => {
  let component: TagEditComponent;
  let fixture: ComponentFixture<TagEditComponent>;
  let tagService: TagService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagEditComponent ],
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        PaginationModule,
        ModalModule.forRoot(),
        SharedModule,
        ColorPickerModule
      ],
      providers: [
        {provide: TagService, useClass: MockTagService},
        {provide: FlashMessageService, useClass: MockFlashMessageService}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagEditComponent);
    component = fixture.componentInstance;
    tagService = TestBed.get(TagService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load tag data on showModal call', async(() => {
    spyOn(tagService, 'getTag').and.callFake(() => {
      return Promise.resolve({id:1, title:"first", description:"first descr"})
    });
    fixture.detectChanges();
    component.showModal(1);
    fixture.whenStable().then(()=>{
      fixture.detectChanges();
      expect(tagService.getTag).toHaveBeenCalledWith(1);
      expect(fixture.nativeElement.querySelector("#title").value).toBe("first");
      expect(fixture.nativeElement.querySelector("#description").value).toBe("first descr");
    });
  }));

  it('should have correct payload for service', fakeAsync(() => {
    spyOn(tagService, 'getTag').and.callThrough();
    spyOn(tagService, 'putTag').and.callThrough();
      component.showModal(1);
      tick(1000);
      fixture.detectChanges();
      component.title.setValue('something else');
      component.description.setValue('something else too');
      component.color.setValue('#4286f4');
      expect(component.tagForm.valid).toBeTruthy();
      component.onSubmit();
      tick(1000);
      expect(tagService.putTag).toHaveBeenCalledWith(1, JSON.stringify({title:"something else", description:"something else too", color:'#4286f4'}));
  }));

  it('form should be valid before changes', () => {
    fixture.detectChanges();
    component.showModal(1);
    fixture.whenStable().then(()=>{
      expect(component.tagForm.valid).toBeTruthy();
    });
  });

  it('form should be valid after changes', () => {
    fixture.detectChanges();
    component.showModal(1);
    component.title.setValue('something else');
    component.description.setValue('something else too');
    fixture.whenStable().then(()=>{
      expect(component.tagForm.valid).toBeTruthy();
    });
  });
});
