import { Injectable } from '@angular/core';
import { URLSearchParams, Headers } from '@angular/http';

import { JwtHttp } from 'angular2-jwt-refresh';
import 'rxjs/add/operator/toPromise';
import { GlobalService } from "app/shared/global.service";

@Injectable()
export class TagService {

    constructor(private authHttp: JwtHttp, private global: GlobalService) {
        if(global) {
            this.baseUrl = this.global.getConfig().baseUrl + 'tags';
        }
    }

    private baseUrl: string;

    getPage(page) {
        let body = new URLSearchParams();
        body.set('_page', page.toString());
        return this.authHttp.get(this.baseUrl, { search : body})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getTag(tagId, recursiveDetails = false) {
        let body = new URLSearchParams();
        if(recursiveDetails) {
            body.set('recursiveDetails', '1');
        }
        return this.authHttp.get(this.baseUrl + '/' + tagId, { search : body})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getTags() {
        return this.authHttp.get(this.baseUrl)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    postTag(tagJson) {
        let myHeader = new Headers();
        myHeader.append('Content-Type', 'application/json');

        return this.authHttp.post(this.baseUrl, tagJson, { headers: myHeader })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    putTag(tagId, tagJson) {
        let myHeader = new Headers();
        myHeader.append('Content-Type', 'application/json');

        return this.authHttp.put(this.baseUrl + '/' + tagId, tagJson, { headers: myHeader })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    deleteTag(tagId) {
        return this.authHttp.delete(this.baseUrl + '/' + tagId)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}