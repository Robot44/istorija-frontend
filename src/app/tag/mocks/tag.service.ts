import { TagService } from '../tag.service';

export class MockTagService extends TagService {
    constructor() {
        super(null, null);
    }

    postTag(tagJson) {
        return Promise.resolve([]);
    }

    getTag(id) {
        return Promise.resolve({id:1, title:"first", description:"first descr", color:'#4286f4'});
    }

    putTag(tagId, tagJson) {
        return Promise.resolve({id:1, title:"first", description:"first descr", color:'#4286f4'});
    }

    getTags() {
        return Promise.resolve([
            {id:1, title:"first", description:"first descr"},
            {id:2, title:"first", description:"first descr"},
            {id:3, title:"first", description:"first descr"},
            {id:4, title:"first", description:"first descr"}
        ]);
    }

    getPage(page) {
        return Promise.resolve({
            tags: [
                {id:1, title:"first", description:"first descr"},
                {id:2, title:"second", description:"second descr"}
            ],
            total: 2,
            limit: 5
        });
    }

    deleteTag(tagId) {
        return Promise.resolve({id:1, title:"first", description:"first descr", color:'#4286f4'});
    }
}