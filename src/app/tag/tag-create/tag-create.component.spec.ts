import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { TagCreateComponent } from './tag-create.component';

import { ModalModule, PaginationModule } from 'ngx-bootstrap';
import { SelectModule } from 'ng-select';

import { TagService } from '../tag.service';
import { MockTagService } from '../mocks/tag.service';
import { FlashMessageService } from '../../shared/flash-message/flash-message.service';
import { MockFlashMessageService } from '../../shared/mock/flash-message.service';

import { Observable } from 'rxjs/Rx';
import { SharedModule } from "app/shared/shared.module";
import { ColorPickerModule } from "ngx-color-picker";

describe('TagCreateComponent', () => {
  let component: TagCreateComponent;
  let fixture: ComponentFixture<TagCreateComponent>;
  let tagService: TagService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagCreateComponent ],
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SelectModule,
        PaginationModule,
        ModalModule.forRoot(),
        SharedModule,
        ColorPickerModule
      ],
      providers: [
        {provide: TagService, useClass: MockTagService},
        {provide: FlashMessageService, useClass: MockFlashMessageService}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagCreateComponent);
    component = fixture.componentInstance;
    tagService = TestBed.get(TagService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form should send correct data', () => {
    spyOn(tagService, 'postTag').and.callFake(() => {
      return Promise.resolve({id:1, title:"first", description:"first descr"})
    });
    spyOn(component.renew, 'emit').and.callThrough();
    component.showModal();
    fixture.detectChanges();
    component.title.setValue('something else');
    component.description.setValue('something else too');
    let element =  fixture.nativeElement.querySelector(".btn.btn-primary");
    fixture.detectChanges();
    element.click();   
    fixture.whenStable().then(()=>{
      expect(tagService.postTag).toHaveBeenCalledWith(JSON.stringify({title:"something else", description:"something else too"}));
      expect(component.renew.emit).toHaveBeenCalledWith(true);
    });
  });

  it('form should be invalid', () => {
    fixture.detectChanges();
    component.showModal();
    fixture.whenStable().then(()=>{
      expect(component.tagForm.valid).toBeFalsy();
    });
  });

  it('form should be valid', () => {
    fixture.detectChanges();
    component.showModal();
    component.title.setValue('something else');
    component.description.setValue('something else too');
    fixture.whenStable().then(()=>{
      expect(component.tagForm.valid).toBeTruthy();
    });
  });
});
