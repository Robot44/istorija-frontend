import { Component, OnInit, Output, EventEmitter, ViewChild, ViewEncapsulation } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';

import { Tag } from '../tag';
import { TagService } from '../tag.service';

import { FlashMessageService } from '../../shared/flash-message/flash-message.service';

import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'tag-create',
  templateUrl: './tag-create.component.html',
  styleUrls: ['./tag-create.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class TagCreateComponent {
    @Output() renew = new EventEmitter<boolean>();
    @ViewChild(ModalDirective) modal : ModalDirective;

    public title: AbstractControl;
    public description: AbstractControl;
    public color: AbstractControl;
    public isLoading = false;
    public tagForm: FormGroup;
    public submitted = false;

    constructor(
        private fb: FormBuilder,
        private tagService: TagService,
        private flashMessageService: FlashMessageService,
    ) {
        this.createForm();
    }

    showModal() {
        this.isLoading = false;
        this.submitted = false;
        this.reset();
        this.modal.show();
    }

    reset() {
        this.tagForm.reset();
        this.color.setValue('rgb(112, 96, 188)');
        this.description.setValue('');
        this.title.setValue('');
    }

    onSubmit() {
        this.submitted = true;
        let model = <Tag>{};
        if (this.tagForm.valid) {
            model.title = this.title.value;
            model.description = this.description.value;
            model.color = this.color.value;

            this.isLoading = true;
            this.tagService.postTag(JSON.stringify(model))
            .then(
                () => {
                    this.flashMessageService.show('Tag created successfully!', 'success');
                    this.modal.hide();
                    this.renew.emit(true);
                }
            )
            .catch(
                () => this.flashMessageService.show('Failed to create tag!', 'error')
            );
        }
    }

    createForm() {
        this.tagForm = this.fb.group({
            title: ['', Validators.compose([Validators.required, Validators.maxLength(40)])],
            description: ['', Validators.compose([Validators.maxLength(255)])],
            color: ['rgb(112, 96, 188)', Validators.required]
        });
        this.title = this.tagForm.controls['title'];
        this.description = this.tagForm.controls['description'];
        this.color = this.tagForm.controls['color'];
    }

    get currentColor() {
        return this.color.value;
    }

    onColorPickerChange(colorCode) {
        this.color.setValue(colorCode);
        this.color.markAsDirty();
        this.color.markAsTouched();
    }
}
