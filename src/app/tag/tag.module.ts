import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ModalModule, PaginationModule } from 'ngx-bootstrap';
import { SelectModule } from 'ng2-select';
import { SharedModule } from "../shared/shared.module";
import { ColorPickerModule } from "ngx-color-picker";

import { TagService } from './tag.service';
import { TagListComponent } from './tag-list/tag-list.component';
import { TagRoutingModule } from './tag.routing';
import { TagCreateComponent } from './tag-create/tag-create.component';
import { TagEditComponent } from './tag-edit/tag-edit.component';

@NgModule({
    imports: [ 
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        ModalModule,
        SelectModule,
        PaginationModule,
        TagRoutingModule,
        SharedModule,
        ColorPickerModule
    ],
    exports: [],
    declarations: [TagListComponent, TagCreateComponent, TagEditComponent],
    providers: [TagService],
})
export class TagModule { }
