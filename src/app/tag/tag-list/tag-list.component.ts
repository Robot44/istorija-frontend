import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { FlashMessageService } from '../../shared/flash-message/flash-message.service';

import { TagService } from '../tag.service';

@Component({
  selector: 'tag-list',
  templateUrl: './tag-list.component.html',
  styleUrls: ['./tag-list.component.scss']
})
export class TagListComponent implements OnInit {

    constructor( 
      private flashMessageService: FlashMessageService,
      private router: Router,
      private tagService: TagService
    ) { }

    public totalItems: number = 0;
    public currentPage: number = 1;
    public itemsPerPage: number = 0;
    public items: Array<any> = [];
    public isLoading: boolean;

    ngOnInit() {
        this.getPage(this.currentPage);
    }

    public getPage(page) {
        this.isLoading = true;
        this.tagService.getPage(page)
        .then(
            data => {
                this.items = data.tags,
                this.totalItems = data.total,
                this.itemsPerPage = data.limit
                this.isLoading = false;
            },
            error => {
                this.flashMessageService.show('Failed to load page!', 'error');
            }
        );

        return page;
    }

    public deleteTag(tagId) {
        this.tagService.deleteTag(tagId)
        .then(
            data => {
                this.refresh();
            },
            error => {
                this.flashMessageService.show('Failed to remove tag!', 'error');
                this.refresh();
            }
        );
    }

    private pageChanged(event: any): void {
        this.getPage(event.page);
    }

    private refresh() {
        this.getPage(this.currentPage);
    }
}
