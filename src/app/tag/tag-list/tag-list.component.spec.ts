import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';

import { ModalModule, PaginationModule } from 'ngx-bootstrap';
import { SelectModule } from 'ng2-select';

import { TagService } from '../tag.service';
import { MockTagService } from '../mocks/tag.service';
import { FlashMessageService } from '../../shared/flash-message/flash-message.service';
import { MockFlashMessageService } from '../../shared/mock/flash-message.service';

import { Observable } from 'rxjs/Rx';

import { TagListComponent } from './tag-list.component';
import { SharedModule } from "app/shared/shared.module";

describe('TagListComponent', () => {
  let component: TagListComponent;
  let fixture: ComponentFixture<TagListComponent>;
  let tagService: TagService;

  let mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };
  
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TagListComponent ],
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SelectModule,
        PaginationModule.forRoot(),
        ModalModule.forRoot(),
        SharedModule
      ],
      providers: [
        {provide: TagService, useClass: MockTagService},
        {provide: FlashMessageService, useClass: MockFlashMessageService},
        {provide: Router, useValue: mockRouter },
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TagListComponent);
    component = fixture.componentInstance;
    tagService = TestBed.get(TagService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have no elements before init', () => {
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelectorAll("tbody tr").length).toBe(0);
  });

  it('should show two elements', async(() => {
    fixture.detectChanges();
    fixture.whenStable().then(()=>{
      fixture.detectChanges();
      expect(fixture.nativeElement.querySelectorAll("tbody tr").length).toBe(2);
    });
  }));

  it('should call service delete on click', async(() => {
    spyOn(tagService, 'deleteTag').and.callThrough();
    fixture.detectChanges();
    fixture.whenStable().then(()=>{
      fixture.detectChanges();
      let element =  fixture.nativeElement.querySelector(".remove");
      element.click();
      fixture.detectChanges();
      expect(tagService.deleteTag).toHaveBeenCalledWith(1);
    });
  }));
});
