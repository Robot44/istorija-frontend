import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params  } from '@angular/router';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';

import { FlashMessageService } from '../shared/flash-message/flash-message.service';
import { CustomValidators } from 'ng2-validation';

import { UserService } from './user.service';

@Component({
    selector: 'reset-confirm',
    templateUrl: 'reset-confirm.component.html',
    styleUrls: ['reset-confirm.component.scss']
})
export class ResetConfirmComponent implements OnInit {
    private submitted: boolean;
    public resetForm: FormGroup;
    public first: AbstractControl;
    public second: AbstractControl;
    public loading: boolean = true;
    public token: string;

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private userService: UserService,
        private flashMessageService: FlashMessageService,
        private fb: FormBuilder
    ) {
        this.createForm();
    }

    ngOnInit() {
        this.route.params.subscribe(
            params => {
                this.loading = false;   
                this.token = params['token'];
            },
            error => {
                this.flashMessageService.show('Failed to confirm reset!', 'error');
                this.router.navigate['/login'];
            }
        );
    }

    onSubmit() {
        this.submitted = true;
        if(this.resetForm.valid) {
            let data = {plainPassword: this.resetForm.value.first};
            this.userService.postReset(this.token, JSON.stringify(data))
                .then(
                    data => {     
                        localStorage.setItem('id_token', data.token);
                        localStorage.setItem('refresh_token', data.refresh_token);
                        this.flashMessageService.show('Password reset!', 'success');
                        this.router.navigateByUrl('');
                    },
                    error => {
                        this.flashMessageService.show('Failed to reset password!', 'error');
                        this.router.navigateByUrl('');
                    }
                );
        }
    }

    createForm() {
        this.resetForm = this.fb.group({
            first: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(20)])],
            second: ['']
        });
        this.first = this.resetForm.controls['first'];
        this.second = this.resetForm.controls['second'];
        this.second.setValidators(Validators.compose([
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(20),
            CustomValidators.equalTo(this.resetForm.controls['first'])
        ]));
        this.second.updateValueAndValidity();
    }
}