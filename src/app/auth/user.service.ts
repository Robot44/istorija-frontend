import { Injectable } from '@angular/core';
import { URLSearchParams, Headers, Http } from '@angular/http';
import { GlobalService } from "app/shared/global.service";

import 'rxjs/add/operator/toPromise';

@Injectable()
export class UserService {
    private baseUrl: string;

    constructor(private http: Http, private global: GlobalService) {
        this.baseUrl = this.global.getConfig().baseUserUrl;
    }

    postRegistration(userJson) {
        let myHeader = new Headers();
        myHeader.append('Content-Type', 'application/json');

        return this.http.post(this.baseUrl, userJson, { headers: myHeader })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getConfirmRegistration(token) {
        return this.http.get(this.baseUrl + "/" + token + "/confirm")
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getReset(email) {
        return this.http.get(this.baseUrl + "/" + email + "/reset")
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    postReset(token, passwordJson) {
        let myHeader = new Headers();
        myHeader.append('Content-Type', 'application/json');
        return this.http.post(this.baseUrl + "/" + token + "/resets", passwordJson, { headers: myHeader })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}