import { Component, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, AbstractControl, FormGroup, Validators } from "@angular/forms";

import { UserService } from './user.service';

import { FlashMessageService } from '../shared/flash-message/flash-message.service';
import { CustomValidators } from 'ng2-validation';

@Component({
    selector: 'reset',
    templateUrl: 'reset.component.html',
    styleUrls: ['reset-confirm.component.scss']
})
export class ResetComponent {
    public resetForm: FormGroup;
    public email: AbstractControl;

    constructor(
        private flashMessageService: FlashMessageService,
        private userService: UserService,
        private router: Router,
        private fb: FormBuilder,
    ) { 
        this.createForm();
    }

    onSubmit() {
        if(this.resetForm.valid) {
            this.userService.getReset(this.email.value)
                .then(
                    data => {
                            this.flashMessageService.show('Reset successfully, check your email!', 'success');
                            this.router.navigate(['login']);
                    },
                    error => {
                        if(error.status == 404) {
                            this.email.setErrors({"exist":true});
                        } else if(error.status == 400) {
                            this.email.setErrors({"sent":true});
                        } else {
                            this.flashMessageService.show('Failed to register!', 'error');
                        }
                    }
            );
        }
    }

    createForm() {
         this.resetForm = this.fb.group({
            email:['', Validators.compose([Validators.required, CustomValidators.email, Validators.maxLength(255)])],
        });
        this.email = this.resetForm.controls['email'];
    }
}