import { Component } from '@angular/core';
import { AuthService } from './auth.service';
import { Router } from '@angular/router';

@Component({
  selector: 'logout',
  template: '<a class="dropdown-item" (click)="logout()"><i class="fa fa-lock"></i> Logout</a>',
  providers: [AuthService]
})
export class LogoutComponent {

  constructor(private auth: AuthService, private router: Router) { }

  logout() {
      this.auth.logout();
      this.router.navigateByUrl('/login');
  }

}
