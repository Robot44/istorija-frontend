import { Component, OnInit } from '@angular/core';
import { AuthService } from './auth.service';
import { FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { FlashMessageService } from '../shared/flash-message/flash-message.service';

@Component({
  templateUrl: 'login.component.html',
  providers: [AuthService]
})
export class LoginComponent implements OnInit {
  private username: string;
  private password: string;
  private loading:boolean = false;

  constructor(
    private auth: AuthService,
    private router: Router,
    private flashMessageService: FlashMessageService
  ) { }

  ngOnInit(): void {
    if(this.auth.loggedIn()) {
      this.router.navigateByUrl('');
    }
  }

  login() {
    this.loading = true;
    this.auth.login(this.username, this.password).then( 
      response => {
        this.loading = false;
        this.router.navigateByUrl('');
      },
      () => { 
        this.loading = false;
        this.flashMessageService.show('Failed to login', 'error');
      }
    );
  }

}
