import { Injectable } from '@angular/core';
import { HttpModule, Http, URLSearchParams, Headers } from '@angular/http';
import { JwtHelper, tokenNotExpired } from 'angular2-jwt';
import 'rxjs/add/operator/map';
import { GlobalService } from "app/shared/global.service";

@Injectable()
export class AuthService {

  constructor(
    private http: Http,
    private global: GlobalService
  ) {}

  jwtHelper: JwtHelper = new JwtHelper();

  loggedIn() {
    if(tokenNotExpired('id_token')) {
      return true;
    } else {
      if(localStorage.getItem('refresh_token')){
        this.refresh().then(
          () => { return true; },
          error => { return false;}
        );
      }
      return false;
    }
  }

  refresh() {
    if(!localStorage['refresh_token']) {
      return Promise.reject('Bad refresh token');
    }

    let myHeader = new Headers();
    myHeader.append('Content-Type', 'application/json');
    return new Promise((resolve, reject) => {
      this.http.post(this.global.getConfig().baseUrl+'refresh', JSON.stringify({refresh_token: localStorage['refresh_token']}), {headers: myHeader})
        .map(res => res.json())
        .subscribe(
          data => {
            localStorage.setItem('id_token', data.token);
            localStorage.setItem('refresh_token', data.refresh_token);
            resolve(data);
          },
          error => {
            localStorage.removeItem('id_token');
            localStorage.removeItem('refresh_token');
            reject(error);
          }
        )
    });
  }

  login(username, password) {
    let body = new URLSearchParams();
    body.set('username', username);
    body.set('password', password);
    return new Promise((resolve, reject) => {
      this.http.post(this.global.getConfig().baseUrl+'login_check', body)
        .map(res => res.json())
        .subscribe(
          data => {
            localStorage.setItem('id_token', data.token);
            localStorage.setItem('refresh_token', data.refresh_token);
            resolve(data);
          },
          error => {
            reject(error)
          }
        ); 
    });
  }

  logout() {
    localStorage.removeItem('id_token');
    localStorage.removeItem('refresh_token');
  }
}