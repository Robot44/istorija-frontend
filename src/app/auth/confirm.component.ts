import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params  } from '@angular/router';
import { FlashMessageService } from '../shared/flash-message/flash-message.service';

import { UserService } from './user.service';

@Component({
    selector: 'confirm',
    template: '<spinner></spinner>',
})
export class ConfirmComponent implements OnInit {
    constructor(
        private router: Router,
        private route: ActivatedRoute,
        private userService: UserService,
        private flashMessageService: FlashMessageService
    ) { }

    private loading: boolean = true;
    private token: string;

    ngOnInit() {
        this.route.params.subscribe(
            params => {
                this.token = params['token'];
                this.confirmRegistration();
            },
            error => {
                this.flashMessageService.show('Failed to confirm email!', 'error');
                this.router.navigate['/login'];
            }
        );
    }

    confirmRegistration() {
        this.userService.getConfirmRegistration(this.token)
            .then(
                data => {
                    localStorage.setItem('id_token', data.token);
                    localStorage.setItem('refresh_token', data.refresh_token);
                    this.flashMessageService.show('Email confirmed!', 'success');
                    this.router.navigateByUrl('');
                },
                error => {
                    this.flashMessageService.show('Failed to confirm email!', 'error');
                    this.router.navigateByUrl('');
                }
            );
    }
}