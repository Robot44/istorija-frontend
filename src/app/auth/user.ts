export interface User {
    username: string;
    plainPassword: string;
    email: string;
}