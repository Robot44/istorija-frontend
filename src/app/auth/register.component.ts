import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';

import { UserService } from './user.service';
import { User } from './user';

import { FlashMessageService } from '../shared/flash-message/flash-message.service';

import { CustomValidators } from 'ng2-validation';

@Component({
    selector: 'register',
    templateUrl: 'register.component.html',
    styleUrls: ['reset-confirm.component.scss']
})
export class RegisterComponent {
    public submitted: boolean;
    public userForm: FormGroup;
    public username: AbstractControl;
    public email: AbstractControl;
    public first: AbstractControl;
    public second: AbstractControl;
    public isLoading: boolean;

    constructor(
        private flashMessageService: FlashMessageService,
        private fb: FormBuilder,
        private userService: UserService,
        private router: Router
    ) {
        this.createForm();
    }

    onSubmit() {
        this.submitted = true;
        if(this.userForm.valid) {
            this.isLoading = true;
            let user = <User>{};
            user.email = this.userForm.value.email;
            user.username = this.userForm.value.username;
            user.plainPassword = this.userForm.value.first;
            this.userService.postRegistration(user)
                .then(
                    data => {
                        this.flashMessageService.show('Registration successfully, check your email!', 'success');
                        this.router.navigate(['login']);
                        this.isLoading = false;
                    },
                    error => {
                        this.isLoading = false;
                        let body = error.json();
                        if( 'children' in body) {
                            let children = body['children'];
                            if('username' in body['children']){
                                let username = 'errors' in children['username'];
                                this.username.setErrors({"notUnique":true});    
                            }
                            if('email' in body['children']){
                                let email = 'errors' in children['email']; 
                                this.email.setErrors({"notUnique":true}); 
                            }
                        } else {
                            this.flashMessageService.show('Failed to register!', 'error');
                        }
                    }
                );  
        }
    }

    createForm() {
        this.userForm = this.fb.group({
            username:['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(10)])],
            email:['', Validators.compose([Validators.required, CustomValidators.email, Validators.maxLength(255)])],
            first: ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(20)])],
            second: ['']
        });
        this.username = this.userForm.controls['username'];
        this.email = this.userForm.controls['email'];
        this.first = this.userForm.controls['first'];
        this.second = this.userForm.controls['second'];
        this.second.setValidators(Validators.compose([
            Validators.required,
            Validators.minLength(3),
            Validators.maxLength(20),
            CustomValidators.equalTo(this.userForm.controls['first'])
        ]));
        this.second.updateValueAndValidity();
    }
}