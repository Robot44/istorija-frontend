import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Http, RequestOptions, Response } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { AuthHttp, AuthConfig } from 'angular2-jwt';
import { UserService } from './user.service';
import { FlashMessageService } from '../shared/flash-message/flash-message.service';
import { JwtConfigService, JwtHttp } from 'angular2-jwt-refresh';
import { CustomFormsModule } from 'ng2-validation'

import { RegisterComponent } from './register.component';
import { ConfirmComponent } from './confirm.component';
import { ResetComponent } from './reset.component';
import { ResetConfirmComponent } from './reset-confirm.component';

import { SharedModule } from '../shared/shared.module';
import { GlobalService } from 'app/shared/global.service';

@NgModule({
  providers: [
    UserService,
    {
      provide: JwtHttp,
      useFactory: getJwtHttp,
      deps: [ Http, RequestOptions, GlobalService ]
    },
    FlashMessageService
  ],
  imports: [
    FormsModule,
    ReactiveFormsModule,
    CommonModule,
    SharedModule,
    CustomFormsModule,
    RouterModule
  ],
  declarations: [
    RegisterComponent,
    ConfirmComponent,
    ResetComponent,
    ResetConfirmComponent
  ],
  exports: [
    RegisterComponent,
    ConfirmComponent,
    ResetComponent,
    ResetConfirmComponent
  ]
})
export class AuthModule {}

export function getJwtHttp(http: Http, options: RequestOptions, global: GlobalService) {
  let jwtOptions = {
    endPoint: global.getConfig().baseUrl+'refresh',
    payload: { refresh_token:localStorage.getItem('refresh_token')},
    beforeSeconds: 600, 
    tokenName: 'refresh_token',
    refreshTokenGetter: (() => localStorage.getItem('refresh_token')),
    tokenSetter: ((res: Response): boolean | Promise<void> => {
      res = res.json();
      if (!res['token'] || !res['refresh_token']) {
        localStorage.removeItem('id_token');
        localStorage.removeItem('refresh_token');
        return false;
      }
      localStorage.setItem('id_token', res['token']);
      localStorage.setItem('refresh_token', res['refresh_token']);

      return true;
    })
  };

  let authConfig = new AuthConfig({
    noJwtError: true,
    globalHeaders: [{'Accept': 'application/json'}],
    tokenGetter: (() => localStorage.getItem('id_token')),
  });

  return new JwtHttp(
    new JwtConfigService(jwtOptions, authConfig),
    http,
    options
  );
}