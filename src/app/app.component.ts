import { Component } from '@angular/core';
import { Http } from '@angular/http';

@Component({
  // tslint:disable-next-line
  selector: 'body',
  template: `
    <flash-message></flash-message>
    <router-outlet></router-outlet>
  `
})
export class AppComponent { }
