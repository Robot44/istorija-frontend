import { Component, OnInit } from '@angular/core';
import { JwtHelper, tokenNotExpired } from 'angular2-jwt';

@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html'
})
export class FullLayoutComponent implements OnInit {

  public disabled: boolean = false;
  public status: {isopen: boolean} = {isopen: false};

  jwtHelper: JwtHelper = new JwtHelper();

  public username = this.jwtHelper.decodeToken(localStorage.getItem('id_token')).username;

  public toggled(open: boolean): void {
  }

  public toggleDropdown($event: MouseEvent): void {
    $event.preventDefault();
    $event.stopPropagation();
    this.status.isopen = !this.status.isopen;
  }

  ngOnInit(): void {}
}
