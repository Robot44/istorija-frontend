import { Component, OnInit,  } from '@angular/core';

import { Router } from '@angular/router';

import { FlashMessageService } from '../../shared/flash-message/flash-message.service';

import { VisualizationService } from './../visualization.service';
import { GlobalService } from 'app/shared/global.service';

import * as clipboard from 'clipboard';

@Component({
    selector: 'visualization-list',
    templateUrl: 'visualization-list.component.html'
})
export class VisualizationListComponent implements OnInit {
    constructor( 
        private flashMessageService: FlashMessageService,
        private router: Router,
        private visualizationService: VisualizationService,
        private globals: GlobalService
    ) { 
        this.visualizationUrl = this.globals.getConfig().host+'/#/visualization/';
    }

    public visualizationUrl: string;
    private totalItems: number = 0;
    private currentPage: number = 1;
    private itemsPerPage: number = 0;
    private items: Array<any> = [];
    public isLoading: boolean;

    ngOnInit() {
        this.isLoading = true;
        this.getPage(this.currentPage);
    }

    private getPage(page) {
        this.isLoading = true;
        this.visualizationService.getPage(page)
        .then(
            data => {
                this.items = data.visualizations;
                this.totalItems = data.total;
                this.itemsPerPage = data.limit;
                this.currentPage = data.page;
                this.isLoading = false;
            },
            error => {
                this.flashMessageService.show('Failed to load page!', 'error');
            }
        );

        return page;
    }

    private deleteVisualization(visualizationId) {
        this.visualizationService.deleteVisualization(visualizationId)
        .then(
            data => {
                this.refresh();
            },
            error => {
                this.flashMessageService.show('Failed to remove visualization!', 'error');
                this.refresh();
            }
        );
    }

    private getVisualizationUrl(visualizationId): string {
        return this.visualizationUrl + visualizationId;
    }

    private pageChanged(event: any): void {
        this.getPage(event.page);
    }

    private refresh() {
        this.getPage(this.currentPage);
    }

}