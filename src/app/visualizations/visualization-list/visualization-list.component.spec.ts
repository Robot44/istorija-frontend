import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';

import { FlashMessageService } from '../../shared/flash-message/flash-message.service';
import { MockFlashMessageService } from '../../shared/mock/flash-message.service';


import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { Router } from '@angular/router';

import { ModalModule } from 'ng2-bootstrap/modal';
import { PaginationModule } from 'ng2-bootstrap/pagination';
import { SharedModule } from "app/shared/shared.module";
import { VisualizationService } from "app/visualizations/visualization.service";
import { MockVisualizationService } from "app/visualizations/mock/visualization.service";
import { VisualizationListComponent } from "app/visualizations/visualization-list/visualization-list.component";
import { GlobalService } from "app/shared/global.service";

describe('VisualizationListComponent', () => {
  let component: VisualizationListComponent;
  let fixture: ComponentFixture<VisualizationListComponent>;
  let visualizationService: VisualizationService;

  let mockRouter = {
    navigate: jasmine.createSpy('navigate')
  };

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizationListComponent ],
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        PaginationModule.forRoot(),
        ModalModule.forRoot(),
        SharedModule
      ],
      providers: [
        {provide: VisualizationService, useClass: MockVisualizationService},
        {provide: FlashMessageService, useClass: MockFlashMessageService},
        {provide: Router, useValue: mockRouter },
        GlobalService
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizationListComponent);
    component = fixture.componentInstance;
    visualizationService = TestBed.get(VisualizationService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have two elements in list', fakeAsync(() => {
    component.ngOnInit();
    tick();
    fixture.detectChanges();
    expect(fixture.nativeElement.querySelectorAll("tbody tr").length).toBe(2);
  }));
  
  it('should call service delete on click', fakeAsync(() => {
    spyOn(visualizationService, 'deleteVisualization').and.callThrough();
    component.ngOnInit();
    tick();
    fixture.detectChanges();
    let element =  fixture.nativeElement.querySelector(".remove");
    element.click();
    tick();
    expect(visualizationService.deleteVisualization).toHaveBeenCalledWith(1);
  }));
});
