import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { VisualizationService } from './../visualization.service';

import { FlashMessageService } from '../../shared/flash-message/flash-message.service';

@Component({
  selector: 'app-visualization-view',
  templateUrl: './visualization-view.component.html',
  styleUrls: ['./visualization-view.component.scss']
})
export class VisualizationViewComponent implements OnInit {

  private id;
  private visualization;

  constructor(
    private route: ActivatedRoute,
    private visualizationService: VisualizationService,
    private flashMessageService: FlashMessageService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
        params => {
            this.id = params['id'];
            this.loadVisualization();
        },
        error => {
            this.flashMessageService.show('Failed to load visualization!', 'error');
        }
    );
  }

  loadVisualization() {
    this.visualizationService.getVisualization(this.id, true)
    .then(
        data => {
            this.visualization = data;
        }
    );
  }

}
