import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizationViewComponent } from './visualization-view.component';

import { NgModule, NO_ERRORS_SCHEMA } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { VisualizationService } from '../visualization.service';
import { MockVisualizationService } from '../mock/visualization.service';

import { FlashMessageService } from '../../shared/flash-message/flash-message.service';
import { MockFlashMessageService } from '../../shared/mock/flash-message.service';

import { Observable } from 'rxjs/Observable';

describe('VisualizationViewComponent', () => {
  let component: VisualizationViewComponent;
  let fixture: ComponentFixture<VisualizationViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizationViewComponent ],
      providers: [
        {provide: FlashMessageService, useClass: MockFlashMessageService},
        {provide: VisualizationService, useClass: MockVisualizationService},
        {
          provide: ActivatedRoute,
          useValue: {
            params: Observable.of({id: 123})
          }
        }
      ],
      schemas: [NO_ERRORS_SCHEMA]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizationViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
