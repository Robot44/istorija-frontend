import { Component, OnInit } from '@angular/core';
import { DomSanitizer, SafeResourceUrl, SafeUrl} from '@angular/platform-browser';
import { ActivatedRoute, Router } from '@angular/router';

import { VisualizationService } from './../visualization.service';

import { FlashMessageService } from '../../shared/flash-message/flash-message.service';
import { GlobalService } from '../../shared/global.service';

@Component({
    selector: 'visualization-preview',
    templateUrl: 'visualization-preview.component.html',
    styleUrls: ['visualization-preview.component.scss']
})
export class VisualizationPreviewComponent {
  private id:string;
  private visualization;
  private url:any = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private flashMessageService: FlashMessageService,
    private sanitizer: DomSanitizer,
    private globals: GlobalService
  ) { }

  ngOnInit() {
    this.route.params.subscribe(
        params => {
            this.id = params['id'];
            let url = this.globals.getConfig().host + '/#/' + this.router.createUrlTree(['visualization', this.id]);
            this.url = this.sanitizer.bypassSecurityTrustResourceUrl(url);
        },
        error => {
            this.flashMessageService.show('Failed to load visualization!', 'error');
        }
    );
  }
}