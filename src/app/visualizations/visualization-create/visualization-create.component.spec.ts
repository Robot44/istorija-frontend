import { async, ComponentFixture, TestBed, fakeAsync, tick, flushMicrotasks } from '@angular/core/testing';

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { ModalModule, PaginationModule } from 'ngx-bootstrap';
import { SelectModule } from 'ng-select';

import { FlashMessageService } from '../../shared/flash-message/flash-message.service';
import { MockFlashMessageService } from '../../shared/mock/flash-message.service';

import { Observable } from 'rxjs/Rx';
import { SharedModule } from "app/shared/shared.module";
import { ColorPickerModule } from "ngx-color-picker";
import { VisualizationCreateComponent } from "app/visualizations/visualization-create/visualization-create.component";
import { VisualizationService } from "app/visualizations/visualization.service";
import { MockVisualizationService } from "app/visualizations/mock/visualization.service";
import { Visualization } from "app/visualizations/visualization";
import { TimelineService } from "app/timelines/timeline.service";
import { MockTimelineService } from "app/timelines/mock/timeline.service";

describe('VisualizationCreateComponent', () => {
  let component: VisualizationCreateComponent;
  let fixture: ComponentFixture<VisualizationCreateComponent>;
  let visualizationService: VisualizationService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VisualizationCreateComponent ],
      imports: [
        CommonModule,
        FormsModule,
        ReactiveFormsModule,
        SelectModule,
        PaginationModule,
        ModalModule.forRoot(),
        SharedModule,
        ColorPickerModule
      ],
      providers: [
        {provide: VisualizationService, useClass: MockVisualizationService},
        {provide: FlashMessageService, useClass: MockFlashMessageService},
        {provide: TimelineService, useClass: MockTimelineService}
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizationCreateComponent);
    component = fixture.componentInstance;
    visualizationService = TestBed.get(VisualizationService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('form should send correct data', fakeAsync(() => {
    let serviceSpy = spyOn(visualizationService, 'postVisualization').and.callThrough();
    let renewSpy = spyOn(component.renew, 'emit').and.callThrough();
    component.showModal();
    tick(10000);
    fixture.detectChanges();
    component.title.setValue('something else');
    component.type.setValue('MH');
    tick();
    fixture.detectChanges();
    component.timelines.setValue([1,2]);
    component.onSubmit();
    tick(10000);
    fixture.detectChanges();
    expect(component.visualizationForm.valid).toBeTruthy();
    expect(serviceSpy).toHaveBeenCalledWith(JSON.stringify({"timelines":[1,2],"title":"something else","type":"MH"}));
    expect(renewSpy).toHaveBeenCalledWith(true);
  }));

  it('form should be invalid', () => {
    fixture.detectChanges();
    component.showModal();
    fixture.whenStable().then(()=>{
      expect(component.visualizationForm.valid).toBeFalsy();
    });
  });

  it('form should be valid', () => {
    fixture.detectChanges();
    component.showModal();
    component.title.setValue('something elsee');
    component.type.setValue('SH');
    fixture.whenStable().then(()=>{
        let timelines = ['1','2','3'];
        component.timelines.setValue('1');
        expect(component.visualizationForm.valid).toBeTruthy();
    });
  });
});
