import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';

import { Visualization } from '../visualization';
import { VisualizationService } from '../visualization.service';

import { TimelineService } from '../../timelines/timeline.service'; 

import { FlashMessageService } from '../../shared/flash-message/flash-message.service';

import { ModalDirective } from 'ngx-bootstrap';
import { SelectComponent } from 'ng-select';

@Component({
    selector: 'visualization-create',
    templateUrl: 'visualization-create.component.html'
})
export class VisualizationCreateComponent {
    constructor(
        private fb: FormBuilder,
        private timelineService: TimelineService,
        private visualizationService: VisualizationService,
        private flashMessageService: FlashMessageService,
    ) {
        this.createForm();
    }

    @Output() renew = new EventEmitter<boolean>();
    @ViewChild(ModalDirective) modal : ModalDirective;

    title: AbstractControl;
    type: AbstractControl;
    timelines: AbstractControl;

    types = Visualization.types;

    availableTimelines: Array<any> = [];
    isLoading: boolean = true;

    visualizationForm: FormGroup;
    submitted = false;
    multi = false;

    showModal() {
        this.isLoading = true;
        this.submitted = false;
        this.timelineService.getTimelines()
        .then(
            data => {             
                this.availableTimelines = [];
                data.forEach(element => {
                    this.availableTimelines.push({value:element.id, label:element.title});
                });
                this.isLoading = false;
            }
        );
        this.createForm();
       
        this.modal.show();
    }

    onSubmit() {
        this.submitted = true;
        let model = new Visualization();
        if (this.visualizationForm.valid) {
            this.isLoading = true;
            model.timelines = [];
            model.title = this.title.value;
            model.type = this.type.value;
            let timelines = this.timelines.value;
            if(Array.isArray(timelines)) { 
                model.timelines = timelines;
            } else {
                model.timelines = [timelines];
            }

            this.visualizationService.postVisualization(JSON.stringify(model))
            .then(
                () => {
                    this.flashMessageService.show('Visualization created successfully!', 'success');
                    this.modal.hide();
                    this.renew.emit(true);
                }
            )
            .catch(
                () => {
                    this.flashMessageService.show('Failed to create visualization!', 'error');
                    this.modal.hide();
                }
            );
        }
    }

    createForm() {
        this.visualizationForm = this.fb.group({
            title: ['', Validators.compose([Validators.required, Validators.maxLength(40)])],
            type: [this.types[0].id, Validators.required],
            timelines: ['', Validators.required]
        });
        
        this.title = this.visualizationForm.controls['title'];
        this.type = this.visualizationForm.controls['type'];
        this.timelines = this.visualizationForm.controls['timelines'];
        const typeChange = this.type.valueChanges;
        typeChange.subscribe(type => {
            let typeElement = this.types.find(x => x.id == type);
            if(typeElement.multiple) {
                this.timelines.setValidators(Validators.compose([Validators.required, Validators.minLength(2)]));
                this.timelines.updateValueAndValidity();
            } else {
                this.timelines.setValidators(Validators.required);
                this.timelines.updateValueAndValidity();
            }
            this.multi = typeElement.multiple;
            this.timelines.setValue('');
        });
    }
}