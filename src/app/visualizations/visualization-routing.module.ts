import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VisualizationListComponent } from './visualization-list/visualization-list.component';
import { VisualizationPreviewComponent } from "./visualization-preview/visualization-preview.component";

const routes: Routes = [
  { path: '', component: VisualizationListComponent },
  { path: 'preview/:id', component: VisualizationPreviewComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VisualizationListRoutingModule { }

export const routedComponents = [VisualizationListComponent];