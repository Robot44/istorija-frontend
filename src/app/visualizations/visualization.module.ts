import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';

import { PaginationModule, ModalModule } from 'ngx-bootstrap';

import { VisualizationListComponent } from './visualization-list/visualization-list.component';
import { VisualizationCreateComponent } from './visualization-create/visualization-create.component';
import { VisualizationEditComponent } from './visualization-edit/visualization-edit.component';
import { VisualizationListRoutingModule } from './visualization-routing.module';
import { VisualizationPreviewComponent } from './visualization-preview/visualization-preview.component';
import { VisualizationService } from './visualization.service';

import { VerticalTimelineComponent } from './charts/vertical-timeline/vertical-timeline.component';

import { TimelineService } from '../timelines/timeline.service'; 
import { ArticlesModule } from '../articles/articles.module';
import { EventModule } from "../events/event.module";
import { MultipleTimelinesComponent } from './charts/multiple-timelines/multiple-timelines.component';
import { VisualizationViewComponent } from './visualization-view/visualization-view.component';
import { HorizontalTimelineComponent } from './charts/horizontal-timeline/horizontal-timeline.component';
import { SharedModule } from 'app/shared/shared.module';

import { MomentDatePipe } from "app/shared/pipes/moment.pipe";
import { SelectModule } from "ng-select";
import { ClipboardModule } from 'ngx-clipboard';

import { DragScrollModule } from 'angular2-drag-scroll';

@NgModule({
    imports: [
        FormsModule,
        CommonModule,
        ReactiveFormsModule,
        VisualizationListRoutingModule,
        ModalModule,
        PaginationModule,
        SelectModule,
        ArticlesModule,
        EventModule,
        SharedModule,
        ClipboardModule.forRoot(),
        DragScrollModule
    ],
    exports: [VisualizationViewComponent, ],
    declarations: [
        VisualizationListComponent,
        VisualizationCreateComponent,
        VisualizationEditComponent,
        VisualizationPreviewComponent,
        VerticalTimelineComponent,
        MultipleTimelinesComponent,
        VisualizationViewComponent,
        HorizontalTimelineComponent
    ],
    providers: [VisualizationService, TimelineService],
})
export class VisualizationModule { }