import { Component, OnInit, Output, EventEmitter, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators, AbstractControl, FormControl } from '@angular/forms';

import { Visualization } from '../visualization';
import { VisualizationService } from '../visualization.service';

import { TimelineService } from '../../timelines/timeline.service'; 

import { FlashMessageService } from '../../shared/flash-message/flash-message.service';
import { ModalDirective } from 'ngx-bootstrap';
import { SelectComponent } from 'ng-select';

@Component({
    selector: 'visualization-edit',
    templateUrl: 'visualization-edit.component.html'
})
export class VisualizationEditComponent {
    constructor(
        private fb: FormBuilder,
        private timelineService: TimelineService,
        private visualizationService: VisualizationService,
        private flashMessageService: FlashMessageService,
    ) {
        this.createForm();
    }

    @Output() renew = new EventEmitter<boolean>();
    @ViewChild(ModalDirective) modal : ModalDirective;

    public title: AbstractControl;
    public type: AbstractControl;
    public timelines: AbstractControl;
    private visualizationId: number = null;
    public visualizationForm: FormGroup;
    private submitted = false;
    private types = Visualization.types;
    private availableTimelines = [];
    private isLoading = false;
    private multi = false;

    showModal(visualizationId) {
        this.isLoading = true;
        this.submitted = false;
        this.visualizationId = visualizationId;
        this.loadOptions().then( () => {
                this.createForm();
                this.visualizationService.getVisualization(visualizationId)
                .then(
                    data => {
                        this.title.setValue(data.title);
                        this.type.setValue(data.type);
                        let dataTimelines = [];
                        data.timelines.forEach(element => {
                            dataTimelines.push(element.id);
                        });
                        this.timelines.setValue(dataTimelines);
                        this.isLoading = false;
                        this.multi = this.types.find(x => x.id == data.type).multiple;
                    }
                );
            }, 
            error => {
                this.flashMessageService.show('Failed to load data!', 'error');
                this.modal.hide();
            }
        );
       
        this.modal.show();
    }

    private loadOptions() {
        return new Promise((resolve, reject) => 
            this.timelineService.getTimelines()
            .then(
                data => {
                    this.availableTimelines = [];
                    data.forEach(element => {
                        this.availableTimelines.push({value:element.id, label:element.title});
                    });
                    resolve(true);
                },
                error => resolve(false)
        ));
    }

    onSubmit() {
        this.submitted = true;
        this.isLoading = true;
        let model = new Visualization;
        if (this.visualizationForm.valid) {
            this.isLoading = true;
            model.timelines = [];
            model.title = this.title.value;
            model.type = this.type.value;
            let timelines = this.timelines.value;
            if(Array.isArray(timelines)) { 
                model.timelines = timelines;
            } else {
                model.timelines = [timelines];
            }
            this.visualizationService.putVisualization(this.visualizationId, JSON.stringify(model))
            .then(
                () => {
                    this.flashMessageService.show('Visualization updated successfully!', 'success');
                    this.modal.hide();
                    this.renew.emit(true);
                }
            )
            .catch(
                () => {
                    this.flashMessageService.show('Failed to update visualization!', 'error');
                    this.modal.hide();
                }
            );
        }
    }

    createForm() {
        this.visualizationForm = this.fb.group({
            title: ['', Validators.compose([Validators.required, Validators.maxLength(40)])],
            type: ['', Validators.required],
            timelines: [[], Validators.required]
        });
        this.title = this.visualizationForm.controls['title'];
        this.type = this.visualizationForm.controls['type'];
        this.timelines = this.visualizationForm.controls['timelines'];
        const typeChange = this.type.valueChanges;
        typeChange.subscribe(type => {
            let typeElement = this.types.find(x => x.id == type);
            if(typeElement.multiple) {
                this.timelines.setValidators(Validators.compose([Validators.required, Validators.minLength(2)]));
                this.timelines.updateValueAndValidity();
            } else {
                this.timelines.setValidators(Validators.required);
                this.timelines.updateValueAndValidity();
            }
            this.multi = typeElement.multiple;
            this.timelines.setValue('');
        });
    }
}