import { Injectable } from '@angular/core';
import { URLSearchParams, Headers, Http } from '@angular/http';

import { JwtHttp } from 'angular2-jwt-refresh';
import 'rxjs/add/operator/toPromise';
import { GlobalService } from "app/shared/global.service";

@Injectable()
export class VisualizationService {

    constructor(
        public authHttp: JwtHttp,
        public http: Http,
        private global: GlobalService      
    ) { 
        if(global) {
            this.baseUrl = global.getConfig().baseUrl + 'visualizations';
        }
    }

    private baseUrl: string;

    getPage(page) {
        let body = new URLSearchParams();
        body.set('_page', page.toString());
        return this.authHttp.get(this.baseUrl, { search : body})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    getVisualization(visualizationId, recursiveDetails = false) {
        let body = new URLSearchParams();
        if(recursiveDetails) {
            body.set('recursiveDetails', '1');
        }
        return this.http.get(this.baseUrl + '/' + visualizationId, { search : body})
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    postVisualization(visualizationJson) {
        let myHeader = new Headers();
        myHeader.append('Content-Type', 'application/json');

        return this.authHttp.post(this.baseUrl, visualizationJson, { headers: myHeader })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    putVisualization(visualizationId, visualizationJson) {
        let myHeader = new Headers();
        myHeader.append('Content-Type', 'application/json');

        return this.authHttp.put(this.baseUrl + '/' + visualizationId, visualizationJson, { headers: myHeader })
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    deleteVisualization(visualizationId) {
        return this.authHttp.delete(this.baseUrl + '/' + visualizationId)
            .toPromise()
            .then(response => response.json())
            .catch(this.handleError);
    }

    private handleError(error: any): Promise<any> {
        return Promise.reject(error.message || error);
    }
}