import { Component, Input, ViewChild } from '@angular/core';

import { Timeline } from '../../../timelines/timeline';

import { EventDetailsComponent } from "../../../events/event-details/event-details.component";

import { GlobalService } from "app/shared/global.service";

@Component({
    selector: 'vertical-timeline',
    templateUrl: 'vertical-timeline.component.html',
    styleUrls: ['vertical-timeline.component.scss']
})
export class VerticalTimelineComponent {
    @Input() timeline: Timeline;
    private url: string;

    constructor(private global: GlobalService) {
        this.url = global.getConfig().api;
    }
}