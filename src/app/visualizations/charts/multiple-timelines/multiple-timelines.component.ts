import { Component, OnInit, OnChanges, ViewChild, ElementRef, Input, ViewEncapsulation } from '@angular/core';

import * as d3 from 'd3';
import * as moment from 'moment';

import { EventDetailsComponent } from "../../../events/event-details/event-details.component";
import { GlobalService } from "app/shared/global.service";
import { Event } from "app/events/event";

@Component({
  selector: 'multiple-timelines',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './multiple-timelines.component.html',
  styleUrls: ['./multiple-timelines.component.scss']
})
export class MultipleTimelinesComponent implements OnInit {
  @Input() public visualization;
  @ViewChild('chart') private chartContainer: ElementRef;
  @ViewChild('eventDetails') private eventDetailsComponent: EventDetailsComponent;
  private events: Array<any>;
  private margin: any = { top: 20, bottom: 20, left: 20, right: 20};
  private chart: any;
  private width: number;
  private height: number;
  private x: any;
  private y: any;
  private xScale: any;
  private yScale: any;
  private colorScale: any;
  private xAxis: any;
  private yAxis: any;
  private svg: any;
  private svgContainer: any;
  private zoom: any;
  private timelines: any;
  private taggedEvents: any;
  private tooltip: any;
  private scalePadding = .25;
  private legendheight: number;
  private baseUrl: string;
  
  constructor(global: GlobalService
  ) { 
    this.baseUrl = global.getConfig().api;
  }

  ngAfterViewInit() {
    this.renewWindowDimensions(this.chartContainer.nativeElement);
    this.onResize();
  }

  ngOnInit() {
    this.processVisualization();
    this.setLegend();
  }

  private onResize() {
    this.setSvg();
    this.setZoom();
    this.setScales();
    this.setTimelines();
    this.setCurve();
    this.setAxis();
    this.setChart();
  }

  renewWindowDimensions(element) {
    let legendHeight = d3.select(".timeline-legend").style('height');
    this.legendheight = +legendHeight.substring(0, legendHeight.length - 2);
    this.width = element.offsetWidth - this.margin.left - this.margin.right;
    this.height = element.offsetHeight - this.margin.top - this.margin.bottom - this.legendheight;
  }

  private setSvg() {
    let element = this.chartContainer.nativeElement;
    this.renewWindowDimensions(element);

    if(this.svg) {
      this.svg.remove();
    }

    this.svg = d3.select(element)
      .append('svg')
      .attr('width', element.offsetWidth)
      .attr('height', element.offsetHeight - this.legendheight);

    this.svgContainer = this.svg
      .append('g')
      .attr("transform", "translate(" + this.margin.left + "," + this.margin.top + ")");

    this.svgContainer.append("defs").append("clipPath")
          .attr("id", "clip")
          .append("rect")
          .attr("width",  this.width)
          .attr("height", this.height);
  }

  setZoom() {
    let element = this.chartContainer.nativeElement;
    this.renewWindowDimensions(element);

    this.zoom = d3.zoom()
      .scaleExtent([1, Infinity])
      .translateExtent([[0, 0], [this.width, this.height]])
      .extent([[0, 0], [this.width, this.height]])
      .on("zoom", () => {
            var transform = d3.event.transform;
            let newScale = transform.rescaleX(this.x);
            this.redraw(newScale);
        }
      );

    this.svgContainer.append("rect")
      .attr("class", "zoom")
      .attr("width", this.width)
      .attr("height", this.height)
      .call(this.zoom);
  }

  private setScales() {
    let element = this.chartContainer.nativeElement;
    this.renewWindowDimensions(element);

    this.x = d3.scaleTime().range([0, this.width]);
    let extent = d3.extent(this.events, (d) => { return d.date; });
    let start = moment(extent[0]);
    let end = moment(extent[1]);
    let buffer = moment.duration(end.diff(start)).asMinutes() * 0.05;
    extent[0] = start.add(-buffer, 'minutes').toDate();
    extent[1] = end.add(buffer, 'minutes').toDate();
    this.xScale = this.x
      .domain(extent);
    
    let domain = [];
    this.timelines.forEach((timeline, index) => {
      domain.push(index);
    });
    
    this.y = d3.scalePoint().range([this.height, 0]);

    this.yScale = this.y
      .domain(domain)
      .padding(this.scalePadding);
  }

  private setAxis() {
    let element = this.chartContainer.nativeElement;
    this.renewWindowDimensions(element);

    this.xAxis = d3.axisBottom(this.x);
    this.svgContainer.append('g')
      .attr('class', 'axis axis-x')
      .attr('transform', `translate(0, ${this.height})`)
      .call(this.xAxis);
  }

  private setTimelines() {
    let me = this;
    let timelineColor = d3.scaleOrdinal(d3.schemeCategory10);
    this.svgContainer.selectAll("line .timeline-line")
      .data(this.timelines)
      .enter()
      .append("line")
      .attr('class', 'timeline-line')
      .attr('x1', (d) => { return this.xScale(new Date(d.range[0])); })
      .attr('x2', (d) => { return this.xScale(new Date(d.range[1])); })
      .attr('y1', (d) => { return this.yScale(d.index); })
      .attr('y2', (d) => { return this.yScale(d.index); })
      .attr('stroke', (d) => { return d.original.color? d.original.color : timelineColor(d.index+4); })
      .attr('stroke-width', 12)
      .attr("clip-path", "url(#clip)")
      .style('opacity', .7)
      .on('mouseover', function(d) {
        let selected = this;
        d3.selectAll('.timeline-line')
          .style('opacity', function() { 
            return (this == selected)? 1: 0.2;
          });
        d3.selectAll('circle')
          .style('opacity', function() {
            return d3.select(this).classed('timeline'+d.index)? 1: 0.2;
          });
        let coords = d3.mouse(this);
        let x:number = coords[0];
        let y:number = coords[1];
        me.drawTooltip(x, y, d, this, me.createTimelineBody);
      })
      .on('mousemove', function(d) {
        me.tooltipMousemove(d, this);
      })
      .on('mouseout', function() {
        d3.selectAll('.timeline-line').style('opacity', 0.7);
        d3.selectAll('circle').style('opacity', 1);
        me.removeTooltip();
      });
  }

  private redraw(xScale) {
    this.svgContainer.select(".axis-x").call(this.xAxis.scale(xScale));
    this.svgContainer.selectAll(".timeline-line")
      .attr('x1', (d) => { return xScale(new Date(d.range[0])) })
      .attr('x2', (d) => { return xScale(new Date(d.range[1])) });
    this.svgContainer.selectAll(".timeline-curve")
      .attr("d", d3.line()
        .curve(d3.curveCardinal)
        .x((d:any) => { return xScale(d.date); })
        .y((d:any) => { return this.yScale(d.timelineIndex); })
      );
    this.svgContainer.selectAll("circle").attr("cx", (d) => { return xScale(d.date) });
  }

  private setCurve() {
    let me = this;
    let line = d3.line().curve(d3.curveCardinal);

    me.taggedEvents.forEach(group => {
      me.svgContainer
        .append('path')
        .data([group.events])
        .attr("d", d3.line().curve(d3.curveCardinal)
          .x((d:any) => { return this.xScale(d.date); })
          .y((d:any) => { return this.yScale(d.timelineIndex); })
        )
        .attr("class", "timeline-curve")
        .style("stroke", (d) => { return group.tag.color? group.tag.color : this.colorScale(group.tag.id); })
        .style("fill", "none")
        .style("stroke-width", "10px")
        .attr("clip-path", "url(#clip)")
        .style('opacity', .7)
        .on("mouseover", function(d) {
          me.tagMouseover(d, group, this);
        })
        .on("mousemove", function(d) {
          me.tooltipMousemove(d, this);
        })
        .on("mouseout", function(d) {
          me.tagMouseout(d);
          me.removeTooltip();
        });
    })
  }

  private setChart() {
    let element = this.chartContainer.nativeElement;
    this.renewWindowDimensions(element);
    let me = this;

    this.svgContainer.selectAll("circle")
      .data(this.events)
      .enter()
      .append("circle")
      .attr("class", (d) => {return "data-point timeline" + d.timelineIndex})
      .attr("cx", (d) => { return this.xScale(d.date) })
      .attr("cy", (d) => { return this.yScale(d.timelineIndex) })
      .attr("r", 8)
      .attr("clip-path", "url(#clip)")
      .on("click", function(d) {
        me.eventDetailsComponent.showModal(d.original);
      })		
      .on('mouseover' , function(d) {	
        d3.select(this).raise();

        let x:number = +d3.select(this).attr('cx'); 
        let y:number = +d3.select(this).attr('cy');
        me.drawTooltip(x, y, d, this, function(tooltip:any, d: any) { return me.createEventBody(tooltip, d, me.baseUrl)});
      })			
      .on("mouseout", function(d) {	
        me.removeTooltip();
      });
  }

  private setLegend() {
    let element = this.chartContainer.nativeElement;
    let timelineColor = d3.scaleOrdinal(d3.schemeCategory10);

    let legend = d3.select(element).append('div')
      .attr('class', 'timeline-legend')
      .selectAll('div.timeline-legend-element')
      .data(this.timelines)
      .enter()
      .append('div')
      .attr('class', 'timeline-legend-element');
    legend.append('div')
      .attr('class', 'timeline-legend-color')
      .style('background', (d:any) => { return d.original.color ? d.original.color: timelineColor(d.index+4);});
    legend.append('div')
      .attr('class', 'timeline-legend-text')
      .html((d:any) => {return `${d.original.title}`});
  }

  private xAdjusted(xPos:number, width:number) {
    let pos = xPos - width/2;
    if(pos < 0) {
      return 0;
    }
    let overflow = pos + width - this.width;
    return overflow > 0 ? pos - overflow: pos;
  }

  private processVisualization() {
    this.colorScale = d3.scaleOrdinal(d3.schemeCategory20);
    this.events = [];
    let me = this;
    this.visualization.timelines.forEach((timeline, index) => {
      if(timeline.events.length > 0) {
        timeline.events.forEach(event => {
          let newEvent = {
            'timelineIndex': index,
            'timeline': timeline.id,
            'date': new Date(event.date),
            'original': event
          };
          // if(newEvent.original.hasOwnProperty('image_url')) {
          //   newEvent.original.image_url = this.global.getConfig().api + newEvent.original.image_url;
          // }
          this.events.push(newEvent);
        });
      }
    });

    this.timelines = [];
    this.visualization.timelines.forEach((timeline, index) => {
      if(timeline.events.length > 0) {
        let range = d3.extent(timeline.events, (d:any) => { return d.date; });
        this.timelines.push({
          'range':range,
          'index':index,
          'original':timeline
        });
      }
    });

    let tags :  { [key:number]: any; } = {};
    this.taggedEvents = [];
    this.events.forEach(element => {
      element.original.tags.forEach(tag => {
        tags[tag.id] = tag;
      });
    });

    for (let key in tags) {
      let tag = tags[key];
      let events = this.events.filter((event) => {
        let hasTag = false;
        event.original.tags.forEach(t => {
          if (t.id == key) hasTag = true;
          return;
        });
        return hasTag;
      });

      events.sort((a:any, b:any) => { 
        return a.date.getTime() - b.date.getTime();
      });

      this.taggedEvents.push({'events':events, 'tag': tag});
    }
  }

  private tagMouseover(d, group, context) {
    let coords = d3.mouse(context);
    let x:number = coords[0];
    let y:number = coords[1];
    d3.selectAll('.timeline-curve')
      .style("opacity", function() {
        return (this == context) ? 1: 0.2;
      })
      .attr('pointer-events', function() {
        return (this == context) ? 'visiblePainted':'none';
      });
    this.drawTooltip(x, y, group, context, this.createTagBody);
  }

  private tooltipMousemove = function(d, context) {
    let coords = d3.mouse(context);
    let x:number = coords[0];
    let y:number = coords[1];
    d3.select('.tooltip-tip')
      .attr('transform', `translate(${(x+10)}, ${y})rotate(180)`);

    let tooltip = d3.select('.tooltipObject');
    let body = d3.select('.tooltipObject .timeline-tooltip');
    let height = body.style('height');
      height = height.substring(0, height.length - 2);
    let width = body.style('width');
      width = width.substring(0, width.length - 2);

    tooltip.attr('y', y - 20 - +height);
    tooltip.attr('x', this.xAdjusted(x, +width)); 
  }

  private tagMouseout(d) {
    d3.selectAll('.timeline-curve')
      .style("opacity", 0.7)
      .attr('pointer-events', 'visiblePainted');
  }

  private drawTooltip(x,y, d, context, createBody:(tooltip:any, d:any)=> any) {
    if(!this.tooltip) {
      this.tooltip = d3.select('svg').select('g')
        .append('foreignObject')
        .attr('class', 'tooltipObject')
        .attr('x', x)
        .attr('y', y)
        .attr('width', 200)
        .attr('height', 100)
        .attr('pointer-events', 'none');
    }

    let arrow = d3.select('svg').select('g')
      .append("polygon")
      .attr("class", "tooltip-tip")
      .attr("points","10,0 20,20 0,20")
      .attr('transform', `translate(${(x+10)}, ${y})rotate(180)`)
      .attr('stroke','#000')
      .attr('fill','#000')
      .attr('pointer-events', 'none');
        
    let body = createBody(this.tooltip, d);
          
    let height = body.style('height');
    height = height.substring(0, height.length - 2);
    this.tooltip.attr('y', y - 20 - +height);
    this.tooltip.attr('height', height);

    let width = body.style('width');
    width = width.substring(0, width.length - 2);
    this.tooltip.attr('x', this.xAdjusted(x, +width)); 
  }

  private createTimelineBody(tooltip, d) {
    let start = moment(d.range[0]).format("Y, MMM D");
    let end = moment(d.range[1]).format("Y, MMM D");
    let body = tooltip.append("xhtml:div")
      .append("div")
      .attr('pointer-events', 'none')
      .attr("class", "timeline-tooltip");
    body.append("div")
      .attr("class", "date")
      .html(`${start} - ${end}`);
    body.append("div")
      .attr("class", "title")
      .html(`${d.original.title}`);

    return body;
  }

  private createEventBody(tooltip, d, baseUrl) {
    let date = moment(d.date).format("Y, MMM D");
    let body = tooltip.append("xhtml:div")
      .append("div")
      .attr('pointer-events', 'none')
      .attr("class", "timeline-tooltip");
    body.append("div")
      .attr("class", "date")
      .html(`${date}`);
    if(d.original.hasOwnProperty('image_url')) {
      body.append("img")
      .attr("src", baseUrl + d.original.image_url)
    }
    body.append("div")
      .attr("class", "title")
      .html(`${d.original.title}`);
    return body;
  }

  private createTagBody(tooltip, d) {
    let body = tooltip.append("xhtml:div")
      .attr('pointer-events', 'none')
      .attr("class", "timeline-tooltip");
    body.append("div")
      .attr("class", "title")
      .html(`${d.tag.title}`);
    body.append("div")
      .attr("class", "description")
      .html(`${d.tag.description}`);

    return body;
  }

  private removeTooltip() {
    d3.select('.tooltip-tip').remove();
    d3.select('.tooltipObject').remove();
    this.tooltip = null;
  }
}
