import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';

import { MultipleTimelinesComponent } from './multiple-timelines.component';
import { GlobalService } from "app/shared/global.service";

describe('MultipleTimelinesComponent', () => {
  let component: MultipleTimelinesComponent;
  let fixture: ComponentFixture<MultipleTimelinesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MultipleTimelinesComponent ],
      schemas: [NO_ERRORS_SCHEMA],
      providers: [
        GlobalService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MultipleTimelinesComponent);
    component = fixture.componentInstance;
    let visualization = JSON.parse('{"id":"c02fe02f-29aa-11e7-91dc-bc5ff4aa81a6","title":"asdf","type":"Multiple","timelines":[{"id":4,"title":"adf","description":"asdf","events":[{"id":61,"date":"2017-04-20 00:00","title":"asdf","short_description":"asdf","long_description":"asdf","articles":[],"tags":[]},{"id":62,"date":"2017-04-23 00:00","title":"asdf","short_description":"asdf","long_description":"asdf","articles":[],"tags":[]}]},{"id":5,"title":"asdf","description":"asdf","events":[{"id":64,"date":"2017-04-20 00:00","title":"asdf","short_description":"asdf","long_description":"asdf","articles":[],"tags":[]},{"id":63,"date":"2017-04-21 00:00","title":"asdf","short_description":"asdf","long_description":"asdf","articles":[],"tags":[]}]}]}');
    component.visualization = visualization;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
