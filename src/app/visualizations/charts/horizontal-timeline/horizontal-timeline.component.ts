import { Component, Input, ViewChild, ElementRef } from '@angular/core';

import { Timeline } from '../../../timelines/timeline';

import { EventDetailsComponent } from "../../../events/event-details/event-details.component";

import { GlobalService } from "app/shared/global.service";

@Component({
  selector: 'horizontal-timeline',
  templateUrl: './horizontal-timeline.component.html',
  styleUrls: ['./horizontal-timeline.component.scss']
})
export class HorizontalTimelineComponent {
  @Input() timeline: Timeline;
  private url: string;

  constructor(private global: GlobalService) {
    this.url = global.getConfig().api;
  }
}
