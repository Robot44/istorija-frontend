import { Injectable } from '@angular/core';
import { URLSearchParams, Headers } from '@angular/http';

import { JwtHttp } from 'angular2-jwt-refresh';

import { VisualizationService } from '../visualization.service';

import 'rxjs/add/operator/toPromise';

@Injectable()
export class MockVisualizationService extends VisualizationService{
    private visualization: string;

    constructor() {
        super(null, null, null);
        this.visualization = JSON.parse('{"id":"c02fe02f-29aa-11e7-91dc-bc5ff4aa81a6","title":"asdf","type":"MH","timelines":[{"id":4,"title":"adf","description":"asdf","events":[{"id":61,"date":"2017-04-20 00:00","title":"asdf","short_description":"asdf","long_description":"asdf","articles":[],"tags":[]},{"id":62,"date":"2017-04-23 00:00","title":"asdf","short_description":"asdf","long_description":"asdf","articles":[],"tags":[]}]},{"id":5,"title":"asdf","description":"asdf","events":[{"id":64,"date":"2017-04-20 00:00","title":"asdf","short_description":"asdf","long_description":"asdf","articles":[],"tags":[]},{"id":63,"date":"2017-04-21 00:00","title":"asdf","short_description":"asdf","long_description":"asdf","articles":[],"tags":[]}]}]}');  
    }

    getVisualization(visualizationId, recursiveDetails = false) {
        return Promise.resolve(this.visualization);
    }

    deleteVisualization(visualizationId) {
        return Promise.resolve(this.visualization);
    }

    getPage(page) {
        return Promise.resolve({
            visualizations: [
                {id:1, title:"first", description:"first descr", type:"SH"},
                {id:2, title:"second", description:"second descr", type:"SH"}
            ],
            page: 1,
            total:2,
            limit: 5
        });
    }

    postVisualization(visualizationJson) {
        return Promise.resolve(this.visualization);
    }

    putVisualization(visualizationId, visualizationJson) {
        return Promise.resolve(this.visualization);
    }
}