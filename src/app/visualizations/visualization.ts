export class Visualization { 
    public title: string;
    public timelines: Array<number>;
    public type: string;
    public static readonly types = [
        {id:"SV", text:'Single vertical', multiple: false}, {id:"SH", text: 'Single horizontal', multiple: false} ,{id:"MH", text:'Multiple Simple', multiple: true}
    ];
}