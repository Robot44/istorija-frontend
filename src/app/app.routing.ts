import { NgModule,  } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';

import { LoginComponent } from './auth/login.component';
import { RegisterComponent } from './auth/register.component';
import { ConfirmComponent } from './auth/confirm.component';
import { ResetComponent } from './auth/reset.component';
import { ResetConfirmComponent } from './auth/reset-confirm.component';
import { AuthGuard } from './auth/auth-guard.service';

import { P404Component } from './pages/404.component';

import { VisualizationViewComponent } from './visualizations/visualization-view/visualization-view.component';

export const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent,
    pathMatch: 'full'
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'confirm/:token',
    component: ConfirmComponent,
  },
  {
    path: 'reset',
    component: ResetComponent
  },
  {
    path: 'reset/:token',
    component: ResetConfirmComponent
  },
  {
    path: 'visualization/:id',
    component: VisualizationViewComponent,
  },
  {
    canActivate: [AuthGuard],
    path: '',
    component: FullLayoutComponent,
    data: {
      title: 'Home'
    },
    children: [
      {
        path: 'timelines',
        loadChildren: './timelines/timelines.module#TimelinesModule'
      },
      {
        path: 'articles',
        loadChildren: './articles/articles.module#ArticlesModule'
      },
      {
        path: 'visualizations',
        loadChildren: './visualizations/visualization.module#VisualizationModule'
      },
      {
        path: 'tags',
        loadChildren: './tag/tag.module#TagModule'
      },
      {
        path: '',
        pathMatch: 'full',
        redirectTo: '/visualizations'
      },
    ]
  },
  {
    path: '404',
    component: P404Component
  },
  {
    path: '**',
    redirectTo: '/404' 
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
