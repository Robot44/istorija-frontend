import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { LocationStrategy, HashLocationStrategy } from '@angular/common';

import { AppComponent } from './app.component';
import { TabsModule, Ng2BootstrapModule, PaginationModule, BsDropdownModule, ModalModule } from 'ngx-bootstrap';
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { NAV_DROPDOWN_DIRECTIVES } from './shared/nav-dropdown.directive';
import { SIDEBAR_TOGGLE_DIRECTIVES } from './shared/sidebar.directive';
import { AsideToggleDirective } from './shared/aside.directive';
import { BreadcrumbsComponent } from './shared/breadcrumb.component';

// Routing Module
import { AppRoutingModule } from './app.routing';

// Layouts
import { FullLayoutComponent } from './layouts/full-layout.component';
import { SimpleLayoutComponent } from './layouts/simple-layout.component';

import { LoginComponent } from './auth/login.component';
import { FormsModule }   from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AuthModule } from './auth/auth.module';
import { AuthGuard } from './auth/auth-guard.service';
import { AuthService } from './auth/auth.service';
import { LogoutComponent } from './auth/logout.component';
import { PagesModule } from './pages/pages.module';
import { ArticlesModule } from './articles/articles.module';
import { VisualizationModule } from './visualizations/visualization.module';
import { TagModule } from './tag/tag.module';
import { SharedModule } from './shared/shared.module';
import { FlashMessageService } from './shared/flash-message/flash-message.service';
import { GlobalService } from './shared/global.service';
import { ClipboardModule } from 'ngx-clipboard';

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    PaginationModule.forRoot(),
    TabsModule.forRoot(),
    ChartsModule,
    HttpModule,
    FormsModule,
    AuthModule,
    PagesModule,
    ArticlesModule,
    VisualizationModule,
    TagModule,
    SharedModule,
    Ng2BootstrapModule.forRoot(),
    BsDropdownModule.forRoot(),
    ModalModule.forRoot()
  ],
  declarations: [
    AppComponent,
    FullLayoutComponent,
    SimpleLayoutComponent,
    NAV_DROPDOWN_DIRECTIVES,
    BreadcrumbsComponent,
    SIDEBAR_TOGGLE_DIRECTIVES,
    AsideToggleDirective,
    LoginComponent,
    LogoutComponent
  ],
  providers: [
    {
      provide: LocationStrategy,
      useClass: HashLocationStrategy
    },
    AuthService,
    AuthGuard,
    FlashMessageService,
    GlobalService
  ],
  bootstrap: [ AppComponent ]
})
export class AppModule { }
