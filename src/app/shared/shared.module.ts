import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { GrowlModule } from 'primeng/primeng';

import { FlashMessageComponent } from './flash-message/flash-message.component';
import { FlashMessageService } from './flash-message/flash-message.service';
import { GlobalService } from "./global.service";

import { SpinnerComponent } from './spinner/spinner.component';
import { MomentDatePipe } from "app/shared/pipes/moment.pipe";

@NgModule({
    imports: [CommonModule, GrowlModule],
    exports: [SpinnerComponent, FlashMessageComponent, MomentDatePipe],
    declarations: [SpinnerComponent, FlashMessageComponent, MomentDatePipe],
    providers: [DatePipe],
})
export class SharedModule { }