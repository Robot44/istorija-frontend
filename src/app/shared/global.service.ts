import { Injectable } from '@angular/core';

@Injectable()
export class GlobalService {
    private config = {
        "host": "http://localhost:8000",
        "api": "http://localhost:8000",
        "baseArticleUrl" : 'http://localhost:8000/api/articles',
        "baseUserUrl": 'http://localhost:8000/api/users',
        "baseUrl" : 'http://localhost:8000/api/'
    };

    constructor() {}

    getConfig() {
        return this.config;
    }
}