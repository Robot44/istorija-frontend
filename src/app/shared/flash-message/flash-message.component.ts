import { Component, OnInit } from '@angular/core';
import { FlashMessageService } from './flash-message.service';

@Component({
    selector: 'flash-message',
    template: `<p-growl [value]="msgs" [life]="4000"></p-growl>`
})
export class FlashMessageComponent {
    constructor(
        private flashMessageService: FlashMessageService
    ) { 
        flashMessageService.show = this.show.bind(this);
    }

    msgs = [];

    show(message:String, type:String) {
        this.msgs.push({severity:type, detail:message});
    }
}