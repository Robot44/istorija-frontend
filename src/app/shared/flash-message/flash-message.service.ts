import { Injectable } from '@angular/core';

@Injectable()
export class FlashMessageService {
    public show:(message:String, type:String) => void;
}
