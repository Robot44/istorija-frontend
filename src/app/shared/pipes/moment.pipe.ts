import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment';

@Pipe({name: 'momentDate'})

export class MomentDatePipe implements PipeTransform {
    transform(value: any, format: string = ""): string {
       return moment.utc(value,'YYYY-MM-DD HH:mm').format(format); 
    }
}