import { MainPage } from './app.po';

describe('core-ui App', function() {
  let page: MainPage;

  beforeEach(() => {
    page = new MainPage();
  });

  it('should display Login page', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Login');
  });

  it('should display visualization page after login', () => {
    page.navigateTo();
    expect(page.submitLoginData()).toContain('Visualization');
  });

  it('should create and remove visualization', () => {
    page.navigateTo();
    expect(page.createNewVisualization()).toBe(3);
    //expect(page.removeVisualization()).toBe(2);
  });
});
