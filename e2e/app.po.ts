import { browser, element, by } from "protractor";

export class MainPage {
  navigateTo() {
    return browser.get('/');
  }

  getParagraphText() {
    return element(by.css('h1')).getText();
  }

 

  submitLoginData() {
    let username = element(by.css('.mb-1 input'));
    let password = element(by.css('.mb-2 input'));
    username.clear();
    username.sendKeys('admin');
    password.clear();
    password.sendKeys('admin');
    let submit = element(by.css('.p-2 button'));
    submit.click();
    return element(by.css('.card-header')).getText();
  }

  createNewVisualization() {
    let button = element(by.css('thead button'));
    button.click();
    let title = element(by.css('.form-group #title'));
    title.clear();
    title.sendKeys('Some new title');
    let types = element(by.css('.form-group #types'));

    this.selectDropdownbyNum('.form-group #types', 2);
    let timelines = element(by.css('select-dropdown'));
    timelines.click();
    this.selectOptionInNgSelect('select-dropdown', 0);
    timelines.click();
    this.selectOptionInNgSelect('select-dropdown', 1);
    element(by.css('.form-actions .btn-primary')).click();
    return element.all(by.css('tbody tr')).count();
  }

  removeVisualization() {
    let button = element(by.css('thead button'));
    button.click();
    let title = element(by.css('.form-group #title'));
    title.clear();
    title.sendKeys('Some new title');
    let types = element(by.css('.form-group #types'));
    let options = types.findElements(by.tagName('option')).then()
    this.selectDropdownbyNum(types, 2);
    let timelines = element(by.css('select-dropdown'));
    timelines.click();
    this.selectOptionInNgSelect(timelines, 0);
    this.selectOptionInNgSelect(timelines, 1);
    element(by.css('.form-actions .btn-primary')).click();
    return element.all(by.css('tbody tr')).count();
  }

  private selectDropdownbyNum = function (css, optionNum ) {
    if (optionNum){
      var options = element(by.css(css)).all(by.tagName('option'))  
        .then(function(options){
          options[optionNum].click();
        });
    }
  };

  private selectOptionInNgSelect(css, optionNum) {
    if (optionNum){
      var options = element(by.css(css)).all(by.css('.option li')) 
        .then(function(options){
          options[optionNum].click();
        });
    }
  }
}
